from __future__ import absolute_import, unicode_literals

import os

DEBUG = True

ALLOWED_HOSTS = ("localhost", "host.docker.internal")
SITE_DOMAIN = "localhost:8000"

ENVIRONMENT = "dev"

CELERY_TASK_DEFAULT_QUEUE = "celery"

DEFAULT_DB = {
    "ENGINE": "django.contrib.gis.db.backends.postgis",
    "NAME": "linnet_dev",
    "USER": "linnet",
    "PASSWORD": "secrets",
    "HOST": "db",
    "PORT": 5432,
    "TEST": {
        "NAME": "test_db",
    },
}


STATIC_URL = "static/"

SUPERUSER_EMAILS = []


AWS_ACCESS_KEY = os.environ.get('AWS_ACCESS_KEY')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = os.environ.get("AWS_STORAGE_BUCKET_NAME")

AWS_S3_REGION_NAME = os.environ.get("AWS_S3_REGION_NAME")
AWS_S3_CUSTOM_DOMAIN = f"{AWS_STORAGE_BUCKET_NAME}.s3.amazonaws.com"