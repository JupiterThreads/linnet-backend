from django.urls import path
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator, OriginValidator
from core.routing import websocket_urlpatterns
from django_channels_jwt_auth_middleware.auth import JWTAuthMiddlewareStack


application = ProtocolTypeRouter(
    {"websocket": JWTAuthMiddlewareStack(URLRouter(websocket_urlpatterns))}
)
