from django.utils.deprecation import MiddlewareMixin
from traders.models import Trader
from django.http import Http404


class TraderMainMiddleware(MiddlewareMixin):

    TRADER_NOT_FOUND = Http404

    def process_view(self, request, view_func, view_args, view_kwargs):

        trader_id = view_kwargs.get("trader_id", None)
        trader = None

        if trader_id is not None:
            try:
                trader = Trader.objects.get(id=trader_id)
            except Trader.DoesNotExist:
                raise self.TRADER_NOT_FOUND(f"No trader found with id {trader_id}")

        request.trader = trader
        request.trader_mode = request.META.get("HTTP_TRADER_MODE", "false") == "true"

        return None
