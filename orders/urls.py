from rest_framework_nested import routers
from django.contrib import admin
from django.urls import path, include
from orders import views


router = routers.SimpleRouter(trailing_slash=False)
router.register(r"orders", views.OrderViewSet, basename="orders")


app_name = "orders"

urlpatterns = [path("", include(router.urls))]
