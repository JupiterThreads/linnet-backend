from django.test import TestCase
from categories.models import Category
from orders.models import OrderItem
from core.tests.testing_utils import populate_fat_hippo


class OrderItemTests(TestCase):
    def setUp(self):
        populate_fat_hippo()
        beef = Category.objects.get(name="Beef")
        self.product = beef.products.get(name="Wild Bill")

    def test_str_representation(self):
        """Test str representation"""

        with_salad = self.product.variations.get(name="With salad")
        name = f"{self.product.name} {with_salad.name}"

        item = OrderItem.objects.create(
            product=self.product,
            product_variation=with_salad,
            quantity=1,
            name=name,
            price=self.product.unit_price.amount,
            total=with_salad.unit_price.amount,
        )
        self.assertEqual(str(item.name), name)
