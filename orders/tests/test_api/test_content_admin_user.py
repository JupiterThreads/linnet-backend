import pytest
from pytest_common_subject import precondition_fixture

from pytest_drf import (
    ViewSetTest,
    Returns200,
    Returns403,
    UsesGetMethod,
    UsesDeleteMethod,
    UsesDetailEndpoint,
    UsesListEndpoint,
    UsesPatchMethod,
    UsesPostMethod,
    AsUser,
)
from pytest_lambda import lambda_fixture, static_fixture
from pytest_drf.util import pluralized
from djangorestframework_camel_case.util import camelize

from orders.models import OrderItem
from orders.serializers import OrderSerializer


def express_order(order):
    data = OrderSerializer(order).data
    return camelize(data)


express_orders = pluralized(express_order)


@pytest.mark.django_db()
class TestAdminUser(ViewSetTest):

    list_url = lambda_fixture(lambda trader: f"/api/traders/{trader.id}/orders")

    detail_url = lambda_fixture(
        lambda trader, order: f"/api/traders/{trader.id}/orders/{order.id}"
    )

    class TestList(UsesGetMethod, UsesListEndpoint, Returns200, AsUser("cadmin_user")):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        def test_it_returns_orders(self, orders, results):
            expected = express_orders(orders)
            actual = results
            assert expected == actual

    class TestCreate(
        UsesPostMethod, UsesListEndpoint, Returns403, AsUser("cadmin_user")
    ):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        data = lambda_fixture(
            lambda trader, amstel, budweiser, peroni: {
                "trader": trader.id,
                "items": [
                    {"product": amstel.id, "name": amstel.name, "quantity": 4},
                    {"product": budweiser.id, "name": budweiser.name, "quantity": 3},
                    {"product": peroni.id, "name": peroni.name, "quantity": 2},
                ],
            }
        )

        initial_order_ids = precondition_fixture(
            lambda pam: set(pam.orders_for_customer.all().values_list("id", flat=True))
        )

        initial_item_ids = precondition_fixture(
            lambda pam: set(
                OrderItem.objects.filter(order__customer=pam).values_list(
                    "id", flat=True
                )
            )
        )

        def test_it_does_not_create(
            self, initial_order_ids, initial_item_ids, pam, json
        ):
            expected = initial_order_ids
            actual = set(pam.orders_for_customer.all().values_list("id", flat=True))
            assert expected == actual

    class TestRetrieve(
        UsesGetMethod, UsesDetailEndpoint, Returns200, AsUser("cadmin_user")
    ):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        def test_it_returns_order(self, order, json):
            expected = express_order(order)
            actual = json
            assert expected == actual

    class TestUpdate(
        UsesPatchMethod, UsesDetailEndpoint, Returns200, AsUser("cadmin_user")
    ):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        data = static_fixture({"status": 30})

        def test_it_updates_order(self, order, json):
            order.refresh_from_db()
            expected = express_order(order)
            actual = json
            assert expected == actual

    class TestDestroy(
        UsesDeleteMethod, UsesDetailEndpoint, Returns403, AsUser("cadmin_user")
    ):
        def test_it_does_not_delete_order(self, order, json):
            pass
