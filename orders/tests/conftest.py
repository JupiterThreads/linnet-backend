import pytest
from pytest_lambda import lambda_fixture
from users.models import User
from traders.models import Trader, TraderGroup
from categories.models import Category
from products.models import Product
from orders.models import OrderItem, Order


michael = lambda_fixture(
    lambda: User.objects.create(
        name="Michael Scott",
        email="michael.scott@dunder.com",
    ),
    autouse=True,
)


trader = lambda_fixture(
    lambda michael: Trader.objects.create(
        name="The office bar", is_visible=True, owner=michael
    ),
    autouse=True,
)


category = lambda_fixture(
    lambda trader: Category.add_root(
        instance=Category(name="Drinks", trader=trader, is_published=True)
    ),
    autouse=True,
)


amstel = lambda_fixture(
    lambda category: Product.objects.create(
        name="Amstel beer", category=category, unit_price="3.86", is_published=True
    ),
    autouse=True,
)

peroni = lambda_fixture(
    lambda category: Product.objects.create(
        name="Peroni beer", category=category, unit_price="4.26", is_published=True
    ),
    autouse=True,
)


budweiser = lambda_fixture(
    lambda category: Product.objects.create(
        name="Budweiser beer", category=category, unit_price="2.26", is_published=True
    ),
    autouse=True,
)


pam = lambda_fixture(
    lambda: User.objects.create(
        name="Pam Beesly",
        email="pam.beesly@dunder.com",
    ),
    autouse=True,
)


draft_items = lambda_fixture(
    lambda trader, peroni, budweiser, amstel: [
        OrderItem(
            product=obj["product"],
            name=obj["name"],
            quantity=obj["quantity"],
            total=obj["total"],
        )
        for obj in [
            {
                "product": peroni,
                "name": peroni.name,
                "quantity": 2,
                "total": peroni.unit_price * 2,
            },
            {
                "product": budweiser,
                "name": budweiser.name,
                "quantity": 1,
                "total": budweiser.unit_price,
            },
            {
                "product": amstel,
                "name": amstel.name,
                "quantity": 3,
                "total": amstel.unit_price * 3,
            },
        ]
    ]
)


@pytest.fixture
def order(trader, pam, draft_items):
    order = Order.objects.create(customer=pam, trader=trader)

    for item in draft_items:
        item.order = order
    OrderItem.objects.bulk_create(draft_items)

    order.total = order.calculate_total()
    order.save()
    return order


@pytest.fixture
def orders(trader, pam, draft_items):
    first_order_items = draft_items[0:2]
    first_order = Order.objects.create(customer=pam, trader=trader)

    for item in first_order_items:
        item.order = first_order
    OrderItem.objects.bulk_create(first_order_items)

    first_order.total = first_order.calculate_total()
    first_order.save()

    second_order_item = draft_items[2:3][0]
    second_order = Order.objects.create(customer=pam, trader=trader)
    second_order_item.order = second_order
    second_order_item.save()

    second_order.total = second_order.calculate_total()
    second_order.save()

    # most recent first as it returns it is sorted -date_created
    return [second_order, first_order]


@pytest.fixture(autouse=True)
def admin_user(trader):
    user = User.objects.create(name="Jan", email="jan.levinson@dunder.com")
    group = TraderGroup.objects.create(name="admin", trader=trader)
    group.user_set.add(user)
    return user


@pytest.fixture(autouse=True)
def cadmin_user(trader):
    user = User.objects.create(name="Stanley Hudson", email="stan.hudson@dunder.com")
    group = TraderGroup.objects.create(name="content_admin", trader=trader)
    group.user_set.add(user)
    return user


@pytest.fixture(autouse=True)
def read_user(trader):
    user = User.objects.create(name="Ryan Howard", email="ryan.howard@dunder.com")
    group = TraderGroup.objects.create(name="read", trader=trader)
    group.user_set.add(user)
    return user
