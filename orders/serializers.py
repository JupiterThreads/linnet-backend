from django_restql.mixins import DynamicFieldsMixin
from django_restql.serializers import NestedModelSerializer
from django_restql.mixins import EagerLoadingMixin
from django_restql.fields import NestedField
from rest_framework import serializers

from users.serializers import UserSerializer
from products.serializers import ProductSerializer, VariationSerializer
from traders.serializers import TraderSerializer

from .models import Order, OrderItem


class RelatedOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = "__all__"


class OrderItemSerializer(NestedModelSerializer):

    product = NestedField(ProductSerializer, accept_pk_only=True)
    product_variation = NestedField(VariationSerializer, required=False, accept_pk=True)
    order = NestedField(RelatedOrderSerializer, write_only=True, required=False)

    class Meta:
        model = OrderItem
        exclude = ("date_created", "date_modified")


class OrderSerializer(NestedModelSerializer):

    customer = UserSerializer(read_only=True, fields=["id", "name", "email"])
    trader = NestedField(
        TraderSerializer, read_only=True, fields=["id", "name", "currency"]
    )
    items = OrderItemSerializer(many=True, read_only=False, required=True)
    status_display = serializers.CharField(
        source="get_status_display", required=False, read_only=True
    )

    class Meta:
        model = Order
        fields = (
            "id",
            "customer",
            "trader",
            "items",
            "status",
            "status_display",
            "date_created",
            "number",
            "customer_note",
            "total",
            "total_currency",
            "total_tax",
            "total_tax_currency",
            "discount_total",
            "discount_total_currency",
        )
        extra_kwargs = {
            "number": {"read_only": True},
            "date_created": {"read_only": True},
        }

    def create(self, validated_data):

        items = validated_data.pop("items")
        order = Order.objects.create(**validated_data)

        order_items = []

        for item in items:
            new_item = OrderItem.set_price_and_total(item)
            order_items.append(OrderItem(order=order, **new_item))

        OrderItem.objects.bulk_create(order_items)
        order.total = order.calculate_total()
        order.save()

        return order
