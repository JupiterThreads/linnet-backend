from django.db import models
from django.utils.translation import ugettext_lazy as _
from djmoney.models.fields import MoneyField
from products.models import Product, Variation
from traders.models import Trader
from users.models import User
from core.utils import unique_order_number_generator
from decimal import Decimal


class Order(models.Model):

    PENDING = 5
    PROCESSING = 10
    ON_HOLD = 20
    COMPLETED = 30
    CANCELLED = 35
    REFUNDED = 40
    FAILED = 45
    TRASH = 50

    STATUS = (
        (PENDING, _("Pending")),
        (PROCESSING, _("Processing")),
        (ON_HOLD, _("On-hold")),
        (COMPLETED, _("Completed")),
        (CANCELLED, _("Cancelled")),
        (REFUNDED, _("Refunded")),
        (FAILED, _("Failed")),
        (TRASH, _("Trash")),
    )

    status = models.PositiveSmallIntegerField(
        choices=STATUS,
        default=PENDING,
    )

    trader = models.ForeignKey(
        Trader, null=True, on_delete=models.SET_NULL, related_name="orders_for_trader"
    )
    customer = models.ForeignKey(
        User, null=True, on_delete=models.SET_NULL, related_name="orders_for_customer"
    )
    number = models.CharField(
        max_length=255,
        default=unique_order_number_generator,
        editable=False,
        unique=True,
    )
    total = MoneyField(
        max_digits=19, decimal_places=2, default=0.00, default_currency="GBP"
    )
    total_tax = MoneyField(
        max_digits=19, decimal_places=2, default=0.00, default_currency="GBP"
    )
    discount_total = MoneyField(
        max_digits=19, decimal_places=2, default=0.00, default_currency="GBP"
    )
    customer_note = models.CharField(max_length=255, blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_completed = models.DateTimeField(blank=True, null=True)
    date_paid = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.number

    def calculate_total(self):

        total = Decimal("0.00")
        for item in self.items.all():
            total += item.total
        return total


class OrderItem(models.Model):

    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="items")
    product = models.ForeignKey(
        Product,
        on_delete=models.SET_NULL,
        related_name="ordered_items",
        null=True,
    )
    product_variation = models.ForeignKey(
        Variation,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="ordered_variation_items",
    )
    name = models.CharField(max_length=255)
    quantity = models.PositiveSmallIntegerField(default=1)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    subtotal = MoneyField(
        max_digits=19, decimal_places=2, default=0.00, default_currency="GBP"
    )
    subtotal_tax = MoneyField(
        max_digits=19, decimal_places=2, default=0.00, default_currency="GBP"
    )
    total = MoneyField(
        max_digits=19, decimal_places=2, default=0.00, default_currency="GBP"
    )
    total_tax = MoneyField(
        max_digits=19, decimal_places=2, default=0.00, default_currency="GBP"
    )
    price = MoneyField(
        max_digits=19, decimal_places=2, default=0.00, default_currency="GBP"
    )

    def __str__(self):
        return self.name

    @staticmethod
    def set_price_and_total(json_item):

        total = Decimal("0.00")

        qty = json_item["quantity"] if "quantity" in json_item else 1

        if "product_variation" in json_item:
            variation = json_item["product_variation"]
            json_item["price"] = variation.unit_price
            total = variation.unit_price * qty
        elif "product" in json_item:
            product = json_item["product"]
            json_item["price"] = product.unit_price
            total = product.unit_price * qty

        json_item["total"] = total
        return json_item
