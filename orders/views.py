from django.shortcuts import get_object_or_404
from core.pagination import CustomCursorPagination
from rest_framework.decorators import action
from rest_framework import viewsets, filters
from rest_access_policy import AccessViewSetMixin
from .serializers import OrderSerializer
from .access_policies import OrderAccessPolicy
from traders.models import Trader


class OrderViewSet(AccessViewSetMixin, viewsets.ModelViewSet):

    serializer_class = OrderSerializer
    access_policy = OrderAccessPolicy
    pagination_class = CustomCursorPagination
    search_fields = ["number", "trader__name"]
    filter_backends = [filters.SearchFilter]
    lookup_field = "id"

    def get_queryset(self):

        if self.request.trader:
            queryset = self.request.trader.orders_for_trader.all().order_by(
                "-date_created"
            )
        else:
            queryset = self.request.user.orders_for_customer.all().order_by(
                "-date_created"
            )

        return self.access_policy.scope_queryset(self.request, queryset)

    def perform_create(self, serializer):
        serializer.save(customer=self.request.user, trader=self.request.trader)
