from rest_access_policy import AccessPolicy


class OrderAccessPolicy(AccessPolicy):

    statements = [
        {
            "action": ["create"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_not_trader_mode",
        },
        {
            "action": ["list"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "view_order_list",
        },
        {
            "action": ["retrieve"],
            "principal": "authenticated",
            "effect": "allow",
            "condition_expression": [
                "(is_customer or is_trader_owner or has_perm:trader.view_all_content)"
            ],
        },
        {
            "action": ["partial_update"],
            "principal": "authenticated",
            "effect": "allow",
            "condition_expression": [
                "(is_trader_owner or has_perm:trader.manage_all_content)"
            ],
        },
    ]

    def view_order_list(self, request, view, action):

        if request.trader:
            return request.trader.owner == request.user or request.user.has_perm(
                "view_all_content", request.trader
            )
        return True

    def is_customer(self, request, view, action):

        order = view.get_object()
        return order.customer == request.user

    def is_not_trader_mode(self, request, view, action):
        return not request.trader_mode

    @classmethod
    def scope_queryset(cls, request, qs):
        return qs
