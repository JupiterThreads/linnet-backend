# from .serializers import CategorySerializer
# from core.notifications.utils import get_data, send_broadcast

# # TODO CATEGORY ORDER


# async def add_category(category):

#     serializer = CategorySerializer(category)
#     content = {
#         "payload": serializer.data,
#         "type": 'ADD_CATEGORY',
#         "trader_id": str(category.trade_account.id)
#     }

#     data = get_data(content)
#     await send_broadcast(data, category.trade_account)


# async def update_category(category):

#     serializer = CategorySerializer(category)
#     content = {
#         "payload": serializer.data,
#         "type": 'UPDATE_CATEGORY',
#         "trader_id": str(category.trade_account.id)
#     }

#     data = get_data(content)
#     await send_broadcast(data, category.trade_account)


# async def remove_category(category):

#     content = {
#         "payload": str(category.id),
#         "type": "REMOVE_CATEGORY",
#         "trader_id": str(category.trade_account.id)
#     }

#     data = get_data(content)
#     await send_broadcast(data, category.trade_account)


# async def update_category_display_order(category, user, old_index, new_index):

#     content = {
#         "payload": {
#             'old_index': old_index,
#             'new_index': new_index,
#         },
#         "type": "UPDATE_CATEGORY_DISPLAY_ORDER",
#         "trader_id": str(category.trade_account.id),
#         "action_user_id": str(user.id),
#     }

#     data = get_data(content)
#     await send_broadcast(data, category.trade_account)
