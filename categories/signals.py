from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver

# # Category signals


# @receiver(post_save, sender=Category)
# def send_category_to_client(sender, instance, created, **kwargs):
#     if created:
#         async_to_sync(add_category)(instance)
#     else:
#         async_to_sync(update_category)(instance)


# @receiver(post_delete, sender=Category)
# def notify_category_removed(sender, instance, **kwargs):
#     async_to_sync(remove_category)(instance)

# # temporarily just assign display order to be next category, will change


# @receiver(pre_save, sender=Category)
# def set_category_display_order(sender, instance, **kwargs):
#     if instance._state.adding:
#         instance.display_order = Category.get_next_display_order(
#             instance.trade_account)
