from django_restql.mixins import DynamicFieldsMixin
from django_restql.serializers import NestedModelSerializer
from rest_framework import serializers
from rest_framework.exceptions import ParseError
from core.utils import save_new_image, update_instance_image
from .models import Category


class CategorySerializer(DynamicFieldsMixin, serializers.ModelSerializer):

    parent_id = serializers.IntegerField(write_only=True, required=False)
    sibling_id = serializers.IntegerField(write_only=True, required=False)

    image_thumbnail = serializers.ImageField(read_only=True)

    class Meta:
        model = Category
        exclude = ("trader",)
        extra_kwargs = {"path": {"required": False}, "depth": {"required": False}}

    def create(self, validated_data):

        parent_id = validated_data.pop("parent_id", None)
        sibling_id = validated_data.pop("sibling_id", None)

        if parent_id and sibling_id:
            raise ParseError(
                detail="You cannot make a category both a child and a sibling"
            )

        draft_category = Category(**validated_data)

        if parent_id and not sibling_id:
            node = Category.get_node(parent_id)
            new_category = node.add_child(instance=draft_category)
        elif sibling_id and not parent_id:
            node = Category.get_node(sibling_id)
            new_category = node.add_sibling(instance=draft_category)
        else:
            new_category = Category.add_root(instance=draft_category)

        image = validated_data.pop("image", None)

        if image is not None:
            new_category = save_new_image(new_category, "image", image)

        return new_category

    def update(self, instance, validated_data):

        image = validated_data.get("image", instance.image)
        instance = update_instance_image(instance, image, "image")
        return super(CategorySerializer, self).update(instance, validated_data)
