from rest_framework import viewsets, filters
from rest_access_policy import AccessViewSetMixin
from .access_policies import CategoryAccessPolicy
from core.pagination import CustomCursorPagination
from .serializers import CategorySerializer


class CategoryViewSet(AccessViewSetMixin, viewsets.ModelViewSet):

    serializer_class = CategorySerializer
    access_policy = CategoryAccessPolicy
    pagination_class = CustomCursorPagination
    search_fields = ["name"]
    filter_backends = [filters.SearchFilter]
    lookup_field = "id"

    def get_queryset(self):
        return self.access_policy.scope_queryset(
            self.request,
            self.request.trader.categories.all(),
        )

    def perform_create(self, serializer):
        serializer.save(trader=self.request.trader)
