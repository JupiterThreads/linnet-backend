from django.db import models
from django.utils import timezone
from treebeard.mp_tree import MP_Node
from traders.models import Trader
from imagekit import processors
from imagekit.processors import ResizeToFill, ResizeToFit, Thumbnail
from imagekit.models import ProcessedImageField, ImageSpecField
from core.utils import image_path


class Category(MP_Node):

    trader = models.ForeignKey(
        Trader, on_delete=models.CASCADE, related_name="categories"
    )
    name = models.CharField(max_length=255)
    is_published = models.BooleanField(default=False)
    description = models.CharField(max_length=255, blank=True, null=True)
    image = ProcessedImageField(
        upload_to=image_path,
        blank=True,
        null=True,
        processors=[processors.Transpose(), ResizeToFit(400, 400, False)],
        format="JPEG",
        max_length=255,
        options={"quality": 80},
    )
    image_thumbnail = ImageSpecField(
        source="image",
        processors=[ResizeToFit(100, 100)],
        format="JPEG",
        options={"quality": 80},
    )
    date_created = models.DateTimeField(default=timezone.now)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["trader", "name"], name="unique_category")
        ]

    def __str__(self):
        return f"{self.trader}: self.name"

    @property
    def base_s3_path(self):
        return f"{self.trader.base_s3_path}/category_{self.id}"

    @staticmethod
    def get_node(node_id):
        return Category.objects.filter(id=node_id).first()
