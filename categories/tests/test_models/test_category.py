from django.test import TestCase
from categories.models import Category
from traders.models import Trader

# from core.tests.testing_utils import refresh_from_db


class CategoryTestCase(TestCase):
    def setUp(self):
        self.account = Trader.objects.create(name="The Great Gatsby")
        self.cat_beers = Category.objects.create(name="Beers", trader=self.account)
        self.cat_cocktails = Category.objects.create(
            name="Cocktails", trader=self.account
        )
        self.cat_spirits = Category.objects.create(name="Spirits", trader=self.account)

    def test_representation(self):
        """Test category representation"""
        self.assertEqual(
            str(self.cat_beers), f"{self.cat_beers.name} ({self.cat_beers.trader.name})"
        )

    def test_base_s3_path(self):
        """Test base s3 path"""
        self.assertEqual(
            self.cat_beers.base_s3_path,
            f"{self.cat_beers.trader.base_s3_path}/category_{self.cat_beers.id}",
        )

    def test_get_next_display_order(self):
        """Test category next display order"""
        self.assertEqual(Category.get_next_display_order(self.account), 3)

    def test_update_category_list_order(self):
        """Test update category list order"""

        self.assertEqual(self.cat_beers.display_order, 0)
        self.assertEqual(self.cat_cocktails.display_order, 1)
        self.assertEqual(self.cat_spirits.display_order, 2)

        category_ids = [
            str(self.cat_spirits.id),
            str(self.cat_beers.id),
            str(self.cat_cocktails.id),
        ]

        Category.update_category_list_display_order(category_ids)

        # refresh_from_db([
        #     self.cat_spirits,
        #     self.cat_beers,
        #     self.cat_cocktails,
        # ])

        self.assertEqual(self.cat_spirits.display_order, 0)
        self.assertEqual(self.cat_beers.display_order, 1)
        self.assertEqual(self.cat_cocktails.display_order, 2)
