import pytest
from pytest_common_subject import precondition_fixture

from pytest_drf import (
    ViewSetTest,
    Returns200,
    Returns201,
    Returns204,
    Returns404,
    UsesGetMethod,
    UsesDeleteMethod,
    UsesDetailEndpoint,
    UsesListEndpoint,
    UsesPatchMethod,
    UsesPostMethod,
    AsUser,
)
from pytest_lambda import lambda_fixture, static_fixture
from pytest_drf.util import pluralized
from categories.models import Category
from categories.serializers import CategorySerializer
from djangorestframework_camel_case.util import camelize


def express_category(category):
    data = CategorySerializer(category).data
    return camelize(data)


express_categories = pluralized(express_category)


@pytest.mark.django_db()
class TestCategoryViewSet(ViewSetTest):

    list_url = lambda_fixture(lambda trader: f"/api/traders/{trader.id}/categories")

    detail_url = lambda_fixture(
        lambda trader, category: f"/api/traders/{trader.id}/categories/{category.id}"
    )

    class TestList(
        UsesGetMethod,
        UsesListEndpoint,
        Returns200,
    ):
        def test_it_returns_published_categories(self, categories, results):
            expected = express_categories(sorted(categories, key=lambda c: c.id))
            actual = sorted(results, key=lambda d: d["id"])
            assert expected == actual

    class TestCreate(UsesPostMethod, UsesListEndpoint, Returns201, AsUser("michael")):
        data = lambda_fixture(
            lambda: {
                "name": "Wines",
                "isPublished": True,
            }
        )

        initial_category_ids = precondition_fixture(
            lambda trader: set(trader.categories.all().values_list("id", flat=True))
        )

        def test_create_new_category(self, initial_category_ids, trader, json):

            expected = initial_category_ids | {json["id"]}
            actual = set(trader.categories.all().values_list("id", flat=True))
            assert expected == actual
            assert "Wines" == json["name"]

    class TestRetrievePublished(
        UsesGetMethod,
        UsesDetailEndpoint,
        Returns200,
    ):

        category = lambda_fixture(
            lambda trader: Category.add_root(
                instance=Category(name="Soft drinks", is_published=True, trader=trader)
            )
        )

        def test_it_returns_category(self, category, json):
            expected = express_category(category)
            actual = json
            assert expected == actual

    class TestRetrieveNonPublished(
        UsesGetMethod,
        UsesDetailEndpoint,
        Returns404,
    ):

        category = lambda_fixture(
            lambda trader: Category.add_root(
                instance=Category(name="Soft drinks", trader=trader)
            )
        )

        def test_it_does_not_return_category(self, category, json):
            pass

    class TestRetrieveNonPublishedForAuthUser(
        UsesGetMethod, UsesDetailEndpoint, Returns200, AsUser("michael")
    ):
        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        category = lambda_fixture(
            lambda trader: Category.add_root(
                instance=Category(name="Soft drinks", trader=trader)
            )
        )

        def test_it_returns_category(self, category, json):
            expected = express_category(category)
            actual = json
            assert expected == actual

    class TestUpdate(
        UsesPatchMethod, UsesDetailEndpoint, Returns200, AsUser("michael")
    ):
        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        category = lambda_fixture(
            lambda trader: Category.add_root(
                instance=Category(name="Gins", trader=trader)
            )
        )

        data = static_fixture({"name": "Rum and Gins"})

        def test_it_updates_category(self, category, json):
            category.refresh_from_db()
            expected = express_category(category)
            actual = json
            assert expected == actual

    class TestDestroy(
        UsesDeleteMethod, UsesDetailEndpoint, Returns204, AsUser("michael")
    ):
        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        category = lambda_fixture(
            lambda trader: Category.add_root(
                instance=Category(name="Gins", trader=trader)
            )
        )

        initial_category_ids = precondition_fixture(
            lambda trader: set(trader.categories.all().values_list("id", flat=True))
        )

        def test_deletes_category(self, initial_category_ids, category, trader):

            expected = initial_category_ids - {category.id}
            actual = set(trader.categories.all().values_list("id", flat=True))
            assert expected == actual
