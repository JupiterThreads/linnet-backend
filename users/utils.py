def get_image_path(instance, filename):

    try:
        path = f"{instance.base_s3_path}/{filename}"
    except AttributeError as error:
        raise error
    return path


def avatar_path(instance, filename):
    return get_image_path(instance, filename)


def update_instance_image(instance, new_image, field_name):
    current_image = getattr(instance, field_name)
    if new_image is None and current_image:
        current_image.delete(save=False)
    elif new_image is not None:
        if new_image.name != current_image.name:
            current_image.delete(save=False)
        setattr(instance, field_name, new_image)
    return instance
