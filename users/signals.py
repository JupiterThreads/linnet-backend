from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver
from .models import User, UserProfile


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_delete, sender=UserProfile)
def delete_avatar(sender, instance, **kwargs):
    if instance.avatar:
        instance.avatar.delete(save=False)
