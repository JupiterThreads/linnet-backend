from rest_access_policy import AccessPolicy


class UserAccessPolicy(AccessPolicy):

    statements = [
        {
            "action": ["create"],
            "principal": "anonymous",
            "effect": "allow",
        },
        {
            "action": ["current_user"],
            "principal": "authenticated",
            "effect": "allow",
        },
        {
            "action": ["update", "partial_update", "destroy"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": "is_request_from_user",
        },
    ]

    def is_request_from_user(self, request, view, action):
        user = view.get_object()
        return user == request.user
