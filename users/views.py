from rest_framework import viewsets
from users.serializers import UserSerializer
from .models import User, UserProfile
from rest_access_policy import AccessViewSetMixin
from .access_policies import UserAccessPolicy
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action


class UserViewSet(AccessViewSetMixin, viewsets.ModelViewSet):
    access_policy = UserAccessPolicy
    serializer_class = UserSerializer
    lookup_field = "id"

    def get_queryset(self):
        return User.objects.all()
        # queryset = User.objects.all()
        # queryset = self.get_serializer_class().setup_eager_loading(queryset)
        # return queryset

    @action(
        methods=["get"], detail=False, url_path="current-user", url_name="current_user"
    )
    def current_user(self, request, **kwargs):
        user_data = UserSerializer(self.request.user).data
        return Response(user_data)


# class UserProfileViewSet(viewsets.ModelViewSet):
#     serializer_class = UserProfileSerializer
#     permission_classes = (UserProfilePermission,)

#     def get_queryset(self):
#         return UserProfile.objects.all()
