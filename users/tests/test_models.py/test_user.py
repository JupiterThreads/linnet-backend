from django.test import TestCase
from users.models import User


class UserTestCase(TestCase):
    user = None

    def setUp(self):
        self.user = User.objects.create(name="Andrew Bond", email="a.bond@gmail.com")

    def test_representation(self):
        """Test the str representation"""
        self.assertEqual(str(self.user), f"{self.user.email} ({self.user.id})")

    def test_base_s3_path(self):
        """Test the base s3 path"""
        self.assertEqual(self.user.base_s3_path, f"linnet/users/{self.user.id}")

    def test_group_name(self):
        """ " Test group name for websockets"""
        self.assertEqual(self.user.group_name, f"user_{self.user.id}")

    def test_is_active(self):
        """Test is active"""
        self.assertEqual(self.user.is_active, True)
