import pytest
from pytest_drf import APIViewTest, Returns200, UsesGetMethod, AsUser, Returns403
from pytest_common_subject import precondition_fixture
from users.models import User

from pytest_drf import (
    ViewSetTest,
    Returns200,
    Returns201,
    Returns204,
    UsesGetMethod,
    UsesDeleteMethod,
    UsesDetailEndpoint,
    UsesListEndpoint,
    UsesPatchMethod,
    UsesPostMethod,
)
from pytest_lambda import lambda_fixture, static_fixture
from pytest_drf.util import url_for, pluralized


def express_user(user):
    return {
        "id": user.id,
        "name": user.name,
        "email": user.email,
        "isStaff": False,
        "isActive": True,
        "profile": None,
    }


express_users = pluralized(express_user)

wallace = lambda_fixture(
    lambda: User.objects.create(
        name="David wallace",
        email="david.wallace@dunder.com",
    ),
    autouse=True,
)


@pytest.mark.django_db()
class TestUserViewSet(ViewSetTest):

    list_url = lambda_fixture(lambda: "/api/users")
    detail_url = lambda_fixture(lambda user: f"/api/users/{user.id}")

    class TestList(UsesGetMethod, UsesListEndpoint, Returns403, AsUser("wallace")):

        users = lambda_fixture(
            lambda: [
                User.objects.create(name=obj["name"], email=obj["email"])
                for obj in [
                    {
                        "name": "Jim Halpert",
                        "email": "jim.halpert@dunder.com",
                    },
                    {
                        "name": "Pam Beesley",
                        "email": "pam.beesley@dunder.com",
                    },
                ]
            ],
            autouse=True,
        )

        def test_it_does_not_returns_users(self, users, json):
            pass

    class TestCreate(
        UsesPostMethod,
        UsesListEndpoint,
        Returns201,
    ):
        data = static_fixture(
            {
                "email": "michael.scott@dunder.com",
                "password": "password123",
                "name": "Michael Scott",
            }
        )

        initial_user_ids = precondition_fixture(
            lambda: set(User.objects.values_list("id", flat=True))
        )

        def test_creates_new_user(self, initial_user_ids, json):

            expected = initial_user_ids | {json["id"]}
            actual = set(User.objects.values_list("id", flat=True))
            assert expected == actual

    class TestRetrieve(UsesGetMethod, Returns200, AsUser("wallace")):

        url = lambda_fixture(lambda: "/api/users/current-user")

        user = lambda_fixture(lambda: User.objects.get(name="David wallace"))

        def test_it_returns_current_user(self, user, json):
            expected = express_user(user)
            actual = json
            assert expected == actual

    class TestUpdate(
        UsesPatchMethod, UsesDetailEndpoint, Returns200, AsUser("wallace")
    ):

        user = lambda_fixture(lambda: User.objects.get(name="David wallace"))

        data = static_fixture(
            {
                "name": "David Wallace is CFO",
                "email": "david.wallace@dunder-mifflin.com",
            }
        )

        def test_it_updates_user(self, user, json):
            user.refresh_from_db()
            expected = express_user(user)
            actual = json
            assert expected == actual

    class TestDestroy(
        UsesDeleteMethod, UsesDetailEndpoint, Returns204, AsUser("wallace")
    ):

        user = lambda_fixture(lambda: User.objects.get(name="David wallace"))

        initial_user_ids = precondition_fixture(
            lambda: set(User.objects.values_list("id", flat=True))
        )

        def test_deletes_user(self, initial_user_ids, user):
            expected = initial_user_ids - {user.id}
            actual = set(User.objects.values_list("id", flat=True))
            assert expected == actual
