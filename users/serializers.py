from rest_framework import serializers
from django_restql.fields import NestedField
from django_restql.mixins import DynamicFieldsMixin
from django_restql.serializers import NestedModelSerializer
from .models import User, UserProfile
from .utils import update_instance_image
from traders.models import TraderGroup


class GroupSerializer(NestedModelSerializer):
    from traders.serializers import TraderSerializer

    trader = NestedField(TraderSerializer, read_only=True, fields=["id", "name"])

    class Meta:
        model = TraderGroup
        exclude = ("permissions",)


class UserProfileSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ("id", "avatar")
        extra_kwargs = {"avatar": {"allow_null": True}}

    def update(self, instance, validated_data):

        avatar = validated_data.get("avatar", instance.avatar)
        instance = update_instance_image(instance, avatar, "avatar")
        instance.save()
        return instance


class UserSerializer(DynamicFieldsMixin, serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)
    groups = GroupSerializer(
        many=True, required=False, read_only=True, source="trader_groups"
    )
    profile = UserProfileSerializer(required=False, read_only=True)

    class Meta:
        model = User
        fields = ("id", "email", "name", "is_active", "password", "profile", "groups")

    def create(self, validated_data):

        password = validated_data.pop("password")
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        return user
