from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.conf import settings
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from .managers import UserManager
from imagekit import processors
from imagekit.processors import ResizeToFill, ResizeToFit, Thumbnail
from imagekit.models import ProcessedImageField, ImageSpecField
from .utils import avatar_path
from traders.models import TraderGroup


class EmailLowerField(models.EmailField):
    def to_python(self, value):
        value = super(EmailLowerField, self).to_python(value)
        if isinstance(value, str):
            return value.lower()
        return value


class User(AbstractBaseUser, PermissionsMixin):

    email = EmailLowerField(_("email address"), unique=True)
    name = models.CharField(_("name"), max_length=255, blank=True)
    date_joined = models.DateTimeField(_("date joined"), auto_now_add=True)
    is_active = models.BooleanField(_("active"), default=True)
    is_staff = models.BooleanField(_("staff status"), default=False)

    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self):
        return "{} ({})".format(self.email, self.id)

    @property
    def group_name(self):
        return "user_{}".format(str(self.id))

    @property
    def trader_groups(self):
        group_names = self.groups.all().values_list("name", flat=True)
        return TraderGroup.objects.filter(name__in=group_names)

    def get_trader_names_for_all_groups(self):
        all_groups = self.trader_groups
        if all_groups:
            return list(all_groups.values_list("trader__name", flat=True))
        return []

    def remove_user_from_trader_groups(self, trader):

        name_regex = trader.group_name_regex
        groups = self.trader_groups.filter(name__regex=name_regex)

        for group in groups:
            group.user_set.remove(self)


class UserProfile(models.Model):

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="profile"
    )
    avatar = ProcessedImageField(
        upload_to=avatar_path,
        blank=True,
        null=True,
        processors=[processors.Transpose(), ResizeToFit(150, 150)],
        format="JPEG",
        options={"quality": 99},
    )

    def __str__(self):
        return self.user

    @property
    def base_s3_path(self):
        return f"linnet/users/{self.id}"
