from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User
from .forms import UserCreationForm, UserChangeForm
from django.utils.translation import ugettext_lazy as _


class CustomUserAdmin(UserAdmin):

    model = User
    add_form = UserCreationForm
    form = UserChangeForm
    add_form_template = "admin/user/add_form.html"

    list_display = ("email", "name", "is_staff", "is_active")
    list_filter = ("is_staff", "is_superuser", "is_active", "groups")
    search_fields = ("name", "email")
    ordering = ("email",)
    fieldsets = (
        (None, {"fields": ("password",)}),
        (_("Personal info"), {"fields": ("name", "email")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("password1", "password2", "email"),
            },
        ),
    )


admin.site.register(User, CustomUserAdmin)
