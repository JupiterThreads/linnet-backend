import random
import uuid


class GroupUserAction:

    ADD = "add"
    REMOVE = "remove"


def image_path(instance, filename):

    try:
        path = f"{instance.base_s3_path}/{filename}"
    except AttributeError as error:
        raise error
    return path


def random_string_generator(n=6):
    return "".join(["{}".format(random.randint(0, 9)) for num in range(0, n)])


def unique_trader_number_generator(instance, n=6):
    new_number = random_string_generator(n)
    _Class = instance.__class__

    qs_exists = _Class.objects.filter(number=new_number).exists()
    if qs_exists:
        return unique_trader_number_generator(instance, n)
    return new_number
