from rest_framework import viewsets, filters
from .serializers import (
    TraderSerializer,
    BusinessTypeSerializer,
    TermsAndConditionsSerializer,
    GroupSerializer,
)
from .models import Trader, BusinessType, TermsAndConditions, TraderGroup
from rest_access_policy import AccessViewSetMixin
from .access_policies import TraderAccessPolicy
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404
from djmoney.settings import CURRENCY_CHOICES
from rest_framework_gis.filters import DistanceToPointOrderingFilter
from users.models import User
from users.serializers import UserSerializer
from rest_framework.exceptions import ValidationError, ParseError
from .utils import GroupUserAction
from core.pagination import CustomCursorPagination


class TraderViewSet(AccessViewSetMixin, viewsets.ModelViewSet):

    serializer_class = TraderSerializer
    access_policy = TraderAccessPolicy
    filter_backends = [filters.SearchFilter]
    search_fields = ["name"]
    pagination_class = CustomCursorPagination
    distance_ordering_filter_field = "location"
    # filter_backends = (DistanceToPointOrderingFilter,)
    lookup_field = "id"

    def perform_create(self, serializer):

        terms_id = self.request.data.get("terms_and_conditions")
        if terms_id is not None:
            terms_obj = get_object_or_404(TermsAndConditions, id=terms_id)
        else:
            raise ValidationError(
                {"detail": "Please provide valid terms and conditions"}
            )

        trader = serializer.save(owner=self.request.user)
        trader.accepted_terms.add(terms_obj)

    def get_queryset(self):

        queryset = Trader.objects.all()
        return self.access_policy.scope_queryset(self.request, queryset)

    # def list(self, request, *args, **kwargs):
    #     queryset = self.filter_queryset(self.get_queryset())
    #     queryset = queryset.filter(is_visible=True)
    #     return super(TraderViewSet, self).list(request, *args, **kwargs)

    @action(
        methods=["get"],
        detail=False,
        url_path="currency-options",
        url_name="currency_options",
    )
    def currency_options(self, request, **kwargs):

        currency_list = [{"value": k, "display_name": v} for k, v in CURRENCY_CHOICES]
        return Response(currency_list)

    @action(
        methods=["get"],
        detail=False,
        url_path="business-types",
        url_name="business_types",
    )
    def business_types(self, request, **kwargs):

        business_types = BusinessType.objects.all().order_by("name")
        serializer = BusinessTypeSerializer(business_types, many=True)
        return Response(serializer.data)

    @action(
        methods=["get"],
        detail=False,
        url_path="terms-conditions",
        url_name="terms_conditions",
    )
    def terms_conditions(self, request, **kwargs):

        terms_conditions = TermsAndConditions.get_latest_conditions()
        serializer = TermsAndConditionsSerializer(terms_conditions)
        return Response(serializer.data)

    @action(
        methods=["get"], detail=True, url_path="search-users", url_name="search_users"
    )
    def search_users(self, request, **kwargs):

        email = self.request.query_params.get("email", "")
        trader = self.get_object()
        current_user_ids = trader.groups.values_list("user", flat=True)
        users = User.objects.filter(email__iexact=email).exclude(
            id__in=current_user_ids
        )
        serializer = UserSerializer(users, many=True, fields=["id", "name", "email"])
        return Response(serializer.data)

    @action(methods=["post", "get"], detail=True, url_path="groups", url_name="groups")
    def groups(self, request, **kwargs):

        trader = self.get_object()

        if request.method == "GET":
            serializer = GroupSerializer(trader.groups, many=True)
            return Response(serializer.data)

        if request.method == "POST":

            group_name = request.data["group_name"]
            action = request.data["action"]
            user_ids = request.data.get("user_ids", [])

            if group_name not in TraderGroup.ALLOWED_GROUP_NAMES:
                raise ParseError(detail="Group name is not valid")

            if user_ids:

                users = User.objects.filter(id__in=user_ids)

                if action == GroupUserAction.ADD:
                    group, _ = TraderGroup.objects.get_or_create(
                        name=group_name, trader=trader
                    )
                    group.add_users(trader, users)

                if action == GroupUserAction.REMOVE:
                    full_group_name = trader.construct_group_name(group_name)
                    group = get_object_or_404(TraderGroup, name=full_group_name)
                    group.user_set.remove(*users)

                serializer = GroupSerializer(group)
                return Response(serializer.data)
