# Generated by Django 3.2 on 2021-12-02 19:47

import core.utils
from django.db import migrations
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('traders', '0002_alter_trader_managers'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trader',
            name='avatar',
            field=imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to=core.utils.image_path),
        ),
        migrations.AlterField(
            model_name='trader',
            name='cover_image',
            field=imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to=core.utils.image_path),
        ),
    ]
