import pytest
from users.models import User
from pytest_lambda import lambda_fixture
from traders.models import TermsAndConditions, Trader, TraderGroup


michael = lambda_fixture(
    lambda: User.objects.create(
        name="Michael Scott",
        email="michael.scott@dunder.com",
    ),
    autouse=True,
)

terms_and_conditions = lambda_fixture(
    lambda: TermsAndConditions.objects.create(version_number=1, content="my terms"),
    autouse=True,
)


trader = lambda_fixture(
    lambda michael: Trader.objects.create(
        name="The office bar", is_visible=True, owner=michael
    ),
)

groups = lambda_fixture(
    lambda trader: [
        TraderGroup.objects.get_or_create(trader=trader, name=name)[0]
        for name in [
            "admin",
            "content_admin",
            "read",
        ]
    ]
)

users = lambda_fixture(
    lambda: [
        User.objects.create(name=obj["name"], email=obj["email"])
        for obj in [
            {"name": "Jim Halpert", "email": "jim.hallpert@dunder.com"},
            {"name": "Dwight Schrute", "email": "dwight.schrute@dunder.com"},
            {"name": "Pam Beesly", "email": "pam.beesly@dunder.com"},
        ]
    ]
)


@pytest.fixture
def create_group(trader, users):
    def create_group(name):
        group, _ = TraderGroup.objects.get_or_create(name=name, trader=trader)
        group.user_set.add(*users)
        return group

    return create_group


@pytest.fixture
def group_users(groups, users):

    groups[0].user_set.add(users[0])
    groups[1].user_set.add(users[1])
    groups[2].user_set.add(users[2])
