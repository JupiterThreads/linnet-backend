from unittest import skip
from django.test import TestCase
from traders.models import Trader
from users.models import User



@skip("Will be changed")
class TradeAccountTests(TestCase):
    def setUp(self):
        owner = User.objects.create(name="Michael Scott", email="m.scott@dunder.com")
        self.account = Trader.objects.create(name="The Office", owner=owner)

    def test_representation(self):
        """Test the str representation"""

        self.assertEqual(
            str(self.account), f"{self.account.name} ({self.account.number})"
        )

    def test_base_s3_path(self):
        """Test the base s3 path"""
        self.assertEqual(self.account.base_s3_path, f"linnet/traders/{self.account.id}")
