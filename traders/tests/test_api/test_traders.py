import pytest
from pytest_common_subject import precondition_fixture

from pytest_drf import (
    ViewSetTest,
    Returns200,
    Returns201,
    Returns204,
    UsesGetMethod,
    UsesDeleteMethod,
    UsesDetailEndpoint,
    UsesListEndpoint,
    UsesPatchMethod,
    UsesPostMethod,
    AsUser,
)
from pytest_lambda import lambda_fixture, static_fixture
from pytest_drf.util import pluralized
from traders.models import Trader
from traders.serializers import TraderSerializer
from djangorestframework_camel_case.util import camelize


def express_trader(trader):
    data = TraderSerializer(trader).data
    return camelize(data)


express_traders = pluralized(express_trader)


@pytest.mark.django_db()
class TestTraderViewSet(ViewSetTest):

    list_url = lambda_fixture(lambda: "/api/traders")

    detail_url = lambda_fixture(lambda trader: f"/api/traders/{trader.id}")

    class TestList(
        UsesGetMethod,
        UsesListEndpoint,
        Returns200,
    ):

        traders = lambda_fixture(
            lambda michael: [
                Trader.objects.create(name=obj["name"], is_visible=True, owner=michael)
                for obj in [{"name": "Bungalow & Bears"}, {"name": "Rose & Crown"}]
            ]
        )

        def test_it_returns_visible_traders(self, traders, results):
            expected = express_traders(sorted(traders, key=lambda t: t.id))
            actual = sorted(results, key=lambda d: d["id"])
            assert expected == actual

    class TestCreate(UsesPostMethod, UsesListEndpoint, Returns201, AsUser("michael")):

        data = lambda_fixture(
            lambda terms_and_conditions: {
                "name": "Rose & Crown",
                "terms_and_conditions": terms_and_conditions.id,
            }
        )

        initial_trader_ids = precondition_fixture(
            lambda: set(Trader.objects.values_list("id", flat=True))
        )

        def test_created_new_trader(self, initial_trader_ids, json):

            expected = initial_trader_ids | {json["id"]}
            actual = set(Trader.objects.values_list("id", flat=True))
            assert expected == actual
            assert "Rose & Crown" == json["name"]

    class TestRetrieve(
        UsesGetMethod, UsesDetailEndpoint, Returns200, AsUser("michael")
    ):
        def test_it_returns_trader(self, trader, json):
            expected = express_trader(trader)
            actual = json
            assert expected == actual

    class TestUpdate(
        UsesPatchMethod, UsesDetailEndpoint, Returns200, AsUser("michael")
    ):

        data = static_fixture(
            {
                "name": "Parks and rec",
            }
        )

        def test_it_updated_trader(self, trader, json):
            trader.refresh_from_db()
            expected = express_trader(trader)
            actual = json
            assert expected == actual

    class TestDestroy(
        UsesDeleteMethod, UsesDetailEndpoint, Returns204, AsUser("michael")
    ):

        initial_trader_ids = precondition_fixture(
            lambda: set(Trader.objects.values_list("id", flat=True))
        )

        def test_deletes_trader(self, initial_trader_ids, trader):
            expected = initial_trader_ids - {trader.id}
            actual = set(Trader.objects.values_list("id", flat=True))
            assert expected == actual
