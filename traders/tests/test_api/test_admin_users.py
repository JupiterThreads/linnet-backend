import pytest
from pytest_common_subject import precondition_fixture

from pytest_drf import (
    ViewSetTest,
    Returns200,
    Returns403,
    UsesGetMethod,
    UsesDeleteMethod,
    UsesDetailEndpoint,
    UsesPatchMethod,
    UsesPostMethod,
    AsUser,
    APIViewTest,
)
from pytest_lambda import lambda_fixture, static_fixture
from pytest_drf.util import pluralized
from djangorestframework_camel_case.util import camelize
from users.models import User
from traders.models import Trader, TraderGroup
from traders.serializers import TraderSerializer, GroupSerializer


def express_trader(trader):
    data = TraderSerializer(trader).data
    return camelize(data)


express_traders = pluralized(express_trader)


def express_group(group):
    data = GroupSerializer(group).data
    return camelize(data)


express_groups = pluralized(express_group)


def express_user(user):
    return {
        "id": user.id,
        "name": user.name,
        "email": user.email,
    }


express_users = pluralized(express_user)


@pytest.fixture(autouse=True)
def admin_user(trader):
    user = User.objects.create(name="Jan", email="jan.levinson@dunder.com")
    group = TraderGroup.objects.create(name="admin", trader=trader)
    group.user_set.add(user)
    return user


group_url = lambda_fixture(lambda trader: f"/api/traders/{trader.id}/groups")


@pytest.mark.django_db()
class TestAdminUser(ViewSetTest):

    list_url = lambda_fixture(lambda: "/api/traders")

    detail_url = lambda_fixture(lambda trader: f"/api/traders/{trader.id}")

    class TestRetrieve(
        UsesGetMethod, UsesDetailEndpoint, Returns200, AsUser("admin_user")
    ):
        def test_it_returns_trader(self, trader, json):
            expected = express_trader(trader)
            actual = json
            assert expected == actual

    class TestUpdate(
        UsesPatchMethod, UsesDetailEndpoint, Returns200, AsUser("admin_user")
    ):

        data = static_fixture(
            {"location": {"type": "Point", "coordinates": [53.38009, -1.47499]}}
        )

        def test_it_updated_trader(self, trader, json):
            trader.refresh_from_db()
            expected = express_trader(trader)
            actual = json
            assert expected == actual

    class TestDestroy(
        UsesDeleteMethod, UsesDetailEndpoint, Returns403, AsUser("admin_user")
    ):

        initial_trader_ids = precondition_fixture(
            lambda: set(Trader.objects.values_list("id", flat=True))
        )

        def test_it_does_not_delete_trader(self, initial_trader_ids, trader):
            pass


@pytest.mark.django_db()
class TestGetGroups(APIViewTest, UsesGetMethod, Returns200, AsUser("admin_user")):

    url = group_url

    def test_it_returns_groups(self, groups, json):
        expected = express_groups(groups)
        actual = json
        assert expected == actual


@pytest.mark.django_db()
class TestAddUsersToContentAdminGroup(
    APIViewTest, UsesPostMethod, Returns200, AsUser("admin_user")
):
    url = group_url

    data = lambda_fixture(
        lambda users: {
            "user_ids": [user.id for user in users],
            "group_name": "content_admin",
            "action": "add",
        }
    )

    def test_it_adds_users_to_group(self, users, trader, json):
        group = TraderGroup.objects.get(name=trader.content_admin_group_name)
        expected = express_group(group)
        actual = json
        assert expected == actual


@pytest.mark.django_db()
class TestRemoveUsersFromContentAdminGroup(
    APIViewTest, UsesPostMethod, Returns200, AsUser("admin_user")
):

    url = group_url

    data = lambda_fixture(
        lambda users: {
            "user_ids": [user.id for user in users],
            "group_name": "content_admin",
            "action": "remove",
        }
    )

    content_admin_group = precondition_fixture(
        lambda create_group: create_group("content_admin")
    )

    def test_it_removes_users_from_group(self, content_admin_group, json):
        content_admin_group.refresh_from_db()
        expected = express_group(content_admin_group)
        actual = json
        actual_users = actual["users"]
        assert expected == actual
        assert len(actual_users) == 0


@pytest.mark.django_db()
class TestSearchUsers(APIViewTest, UsesGetMethod, Returns200, AsUser("admin_user")):

    url = lambda_fixture(lambda trader: f"/api/traders/{trader.id}/search-users")

    new_user = precondition_fixture(
        lambda: User.objects.create(
            name="Angela Martin", email="angela.martin@dunder.com"
        )
    )

    data = lambda_fixture(lambda new_user: {"email": new_user.email})

    def test_it_returns_found_user(self, group_users, new_user, json):
        expected = [express_user(new_user)]
        actual = json
        assert expected == actual
