import pytest
from pytest_common_subject import precondition_fixture

from pytest_drf import Returns200, UsesGetMethod, UsesPostMethod, AsUser, APIViewTest
from pytest_lambda import lambda_fixture
from pytest_drf.util import pluralized
from traders.models import TraderGroup
from traders.serializers import GroupSerializer
from djangorestframework_camel_case.util import camelize


def express_group(group):
    data = GroupSerializer(group).data
    return camelize(data)


express_groups = pluralized(express_group)


def express_user(user):
    return {
        "id": user.id,
        "name": user.name,
        "email": user.email,
    }


express_users = pluralized(express_user)

group_url = lambda_fixture(lambda trader: f"/api/traders/{trader.id}/groups")


@pytest.mark.django_db()
class TestGroupsList(APIViewTest, UsesGetMethod, Returns200, AsUser("michael")):

    url = group_url

    def test_it_returns_groups(self, groups, json):

        expected = express_groups(groups)
        actual = json
        assert expected == actual


@pytest.mark.django_db()
class TestAddUsersToAdminGroup(
    APIViewTest, UsesPostMethod, Returns200, AsUser("michael")
):
    url = group_url

    data = lambda_fixture(
        lambda users: {
            "user_ids": [user.id for user in users],
            "group_name": "admin",
            "action": "add",
        }
    )

    def test_it_adds_users_to_group(self, users, trader, json):
        group = TraderGroup.objects.get(name=trader.admin_group_name)
        expected = express_group(group)
        actual = json
        expected_users = express_users(sorted(users, key=lambda u: u.id))
        actual_users = actual["users"]
        assert expected == actual
        assert expected_users == actual_users


@pytest.mark.django_db()
class TestRemoveUsersFromAdminGroup(
    APIViewTest, UsesPostMethod, Returns200, AsUser("michael")
):

    url = group_url

    data = lambda_fixture(
        lambda users: {
            "user_ids": [user.id for user in users],
            "group_name": "admin",
            "action": "remove",
        }
    )

    admin_group = precondition_fixture(lambda create_group: create_group("admin"))

    def test_it_removes_users_from_group(self, admin_group, json):
        admin_group.refresh_from_db()
        expected = express_group(admin_group)
        actual = json
        actual_users = actual["users"]
        assert expected == actual
        assert len(actual_users) == 0


@pytest.mark.django_db()
class TestAddUsersToContentGroup(
    APIViewTest, UsesPostMethod, Returns200, AsUser("michael")
):
    url = group_url

    data = lambda_fixture(
        lambda users: {
            "user_ids": [user.id for user in users],
            "group_name": "content_admin",
            "action": "add",
        }
    )

    def test_it_adds_users_to_group(self, users, trader, json):
        group = TraderGroup.objects.get(name=trader.content_admin_group_name)
        expected = express_group(group)
        actual = json
        expected_users = express_users(sorted(users, key=lambda u: u.id))
        actual_users = actual["users"]
        assert expected == actual
        assert expected_users == actual_users


@pytest.mark.django_db()
class TestRemoveUsersFromContentGroup(
    APIViewTest, UsesPostMethod, Returns200, AsUser("michael")
):

    url = group_url

    data = lambda_fixture(
        lambda users: {
            "user_ids": [users[0].id],
            "group_name": "content_admin",
            "action": "remove",
        }
    )

    cadmin_group = precondition_fixture(
        lambda create_group: create_group("content_admin")
    )

    def test_it_removes_users_from_group(self, users, cadmin_group, json):
        cadmin_group.refresh_from_db()
        expected = express_group(cadmin_group)
        actual = json
        actual_users = actual["users"]
        expected_users = express_users(sorted(users[1:3], key=lambda u: u.id))
        assert expected == actual
        assert len(actual_users) == 2
        assert actual_users == expected_users


@pytest.mark.django_db()
class TestAddUsersToReadGroup(
    APIViewTest, UsesPostMethod, Returns200, AsUser("michael")
):
    url = group_url

    data = lambda_fixture(
        lambda users: {
            "user_ids": [user.id for user in users],
            "group_name": "read",
            "action": "add",
        }
    )

    def test_it_adds_users_to_group(self, users, trader, json):
        group = TraderGroup.objects.get(name=trader.read_group_name)
        expected = express_group(group)
        actual = json
        expected_users = express_users(sorted(users, key=lambda u: u.id))
        actual_users = actual["users"]
        assert expected == actual
        assert expected_users == actual_users


@pytest.mark.django_db()
class TestRemoveUsersFromReadGroup(
    APIViewTest, UsesPostMethod, Returns200, AsUser("michael")
):

    url = group_url

    data = lambda_fixture(
        lambda users: {
            "user_ids": [users[0].id, users[1].id],
            "group_name": "read",
            "action": "remove",
        }
    )

    read_group = precondition_fixture(lambda create_group: create_group("read"))

    def test_it_removes_users_from_group(self, users, read_group, json):
        read_group.refresh_from_db()
        expected = express_group(read_group)
        actual = json
        actual_users = actual["users"]
        expected_users = [express_user(users[2])]
        assert expected == actual
        assert len(actual_users) == 1
        assert actual_users == expected_users
