import pytest
from pytest_drf import APIViewTest, AsUser, Returns200, UsesGetMethod, Returns401
from pytest_lambda import lambda_fixture

from traders.serializers import BusinessTypeSerializer, TermsAndConditionsSerializer
from pytest_drf.util import pluralized
from djangorestframework_camel_case.util import camelize
from traders.models import BusinessType, TermsAndConditions
from djmoney.settings import CURRENCY_CHOICES


def express_business_type(b_types):
    data = BusinessTypeSerializer(b_types).data
    return camelize(data)


express_business_types = pluralized(express_business_type)


def express_terms_conditions(terms):
    data = TermsAndConditionsSerializer(terms).data
    return camelize(data)


def express_currencies(currencies):
    return camelize(currencies)


business_types = lambda_fixture(
    lambda: [
        BusinessType.objects.create(name=name)
        for name in [
            "bar",
            "pub" "restaurant",
        ]
    ]
)

currency_options = lambda_fixture(
    lambda: [{"value": k, "display_name": v} for k, v in CURRENCY_CHOICES]
)

terms = lambda_fixture(
    lambda: TermsAndConditions.objects.create(version_number=1.1, content="my terms")
)

btype_url = lambda_fixture(lambda: "/api/traders/business-types")

terms_url = lambda_fixture(lambda: "/api/traders/terms-conditions")

currency_url = lambda_fixture(lambda: "/api/traders/currency-options")


@pytest.mark.django_db()
class TestBusinessTypes(APIViewTest, UsesGetMethod, Returns200, AsUser("michael")):

    url = btype_url

    def test_it_returns_business_types(self, business_types, json):

        expected = express_business_types(business_types)
        actual = json
        assert expected == actual


@pytest.mark.django_db()
class TestBusinessTypesAnonUser(
    APIViewTest,
    UsesGetMethod,
    Returns401,
):
    url = btype_url

    def test_it_does_not_returns_business_types(self, business_types, json):
        pass


@pytest.mark.django_db()
class TestTermsConditions(APIViewTest, UsesGetMethod, Returns200, AsUser("michael")):
    url = terms_url

    def test_it_returns_terms(self, terms, json):

        expected = express_terms_conditions(terms)
        actual = json
        assert expected == actual


@pytest.mark.django_db()
class TestTermsConditionsAnonUser(
    APIViewTest,
    UsesGetMethod,
    Returns401,
):

    url = terms_url

    def test_it_does_not_returns_terms(self, terms, json):
        pass


@pytest.mark.django_db()
class TestCurrencies(APIViewTest, UsesGetMethod, Returns200, AsUser("michael")):
    url = currency_url

    def test_it_returns_currencies(self, currency_options, json):
        expected = express_currencies(currency_options)
        actual = json
        assert expected == actual


@pytest.mark.django_db()
class TestCurrenciesAnonUser(
    APIViewTest,
    UsesGetMethod,
    Returns401,
):
    url = currency_url

    def test_it_does_not_return_currencies(self, currency_options, json):
        pass
