import pytest
from pytest_common_subject import precondition_fixture

from pytest_drf import (
    ViewSetTest,
    Returns200,
    Returns201,
    Returns204,
    Returns403,
    UsesGetMethod,
    UsesDeleteMethod,
    UsesDetailEndpoint,
    UsesListEndpoint,
    UsesPatchMethod,
    UsesPostMethod,
    AsUser,
    APIViewTest,
)
from pytest_lambda import lambda_fixture, static_fixture
from pytest_drf.util import pluralized
from djangorestframework_camel_case.util import camelize
from users.models import User
from traders.models import Trader, TraderGroup
from traders.serializers import TraderSerializer, GroupSerializer


def express_trader(trader):
    data = TraderSerializer(trader).data
    return camelize(data)


express_traders = pluralized(express_trader)


def express_group(group):
    data = GroupSerializer(group).data
    return camelize(data)


express_groups = pluralized(express_group)


@pytest.fixture(autouse=True)
def read_user(trader):
    user = User.objects.create(
        name="Meredith Palmer", email="meredith.palmer@dunder.com"
    )
    group = TraderGroup.objects.create(name="read", trader=trader)
    group.user_set.add(user)
    return user


group_url = lambda_fixture(lambda trader: f"/api/traders/{trader.id}/groups")


@pytest.mark.django_db()
class TestReadUser(ViewSetTest):

    list_url = lambda_fixture(lambda: "/api/traders")

    detail_url = lambda_fixture(lambda trader: f"/api/traders/{trader.id}")

    class TestRetrieve(
        UsesGetMethod, UsesDetailEndpoint, Returns403, AsUser("read_user")
    ):
        def test_it_does_not_returns_trader(self, trader, json):
            pass

    class TestUpdate(
        UsesPatchMethod, UsesDetailEndpoint, Returns403, AsUser("read_user")
    ):

        data = static_fixture(
            {"location": {"type": "Point", "coordinates": [53.38009, -1.47499]}}
        )

        def test_it_does_not_update_trader(self, trader, json):
            pass

    class TestDestroy(
        UsesDeleteMethod, UsesDetailEndpoint, Returns403, AsUser("read_user")
    ):

        initial_trader_ids = precondition_fixture(
            lambda: set(Trader.objects.values_list("id", flat=True))
        )

        def test_it_does_not_delete_trader(self, initial_trader_ids, trader):
            pass


@pytest.mark.django_db()
class TestGetGroups(APIViewTest, UsesGetMethod, Returns403, AsUser("read_user")):

    url = group_url

    def test_it_does_not_returns_groups(self, groups, json):
        pass


@pytest.mark.django_db()
class TestAddUsersToContentAdminGroup(
    APIViewTest, UsesPostMethod, Returns403, AsUser("read_user")
):
    url = group_url

    data = lambda_fixture(
        lambda users: {
            "user_ids": [user.id for user in users],
            "group_name": "read",
            "action": "add",
        }
    )

    def test_it_does_not_adds_users_to_group(self, users, trader, json):
        pass


@pytest.mark.django_db()
class TestRemoveUsersFromContentAdminGroup(
    APIViewTest, UsesPostMethod, Returns403, AsUser("read_user")
):

    url = group_url

    data = lambda_fixture(
        lambda users: {
            "user_ids": [user.id for user in users],
            "group_name": "read",
            "action": "remove",
        }
    )

    read_group = precondition_fixture(lambda create_group: create_group("read"))

    def test_it_removes_users_from_group(self, read_group, json):
        read_group.refresh_from_db()
        users = read_group.user_set.all()
        assert len(users) == 4


@pytest.mark.django_db()
class TestSearchUsers(APIViewTest, UsesGetMethod, Returns403, AsUser("read_user")):

    url = lambda_fixture(lambda trader: f"/api/traders/{trader.id}/search-users")

    new_user = precondition_fixture(
        lambda: User.objects.create(
            name="Angela Martin", email="angela.martin@dunder.com"
        )
    )

    data = lambda_fixture(lambda new_user: {"email": new_user.email})

    def test_it_does_not_returns_found_user(self, group_users, new_user, json):
        pass
