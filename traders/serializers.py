from rest_framework import serializers
from rest_framework_gis.serializers import GeoModelSerializer
from rest_framework_gis.fields import GeometryField
from django_restql.mixins import DynamicFieldsMixin
from django_restql.serializers import NestedModelSerializer

from .models import Trader, BusinessType, AcceptedTerms, TermsAndConditions, TraderGroup
from django_restql.fields import DynamicSerializerMethodField
from django_restql.fields import NestedField
from users.models import User


class TermsAndConditionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TermsAndConditions
        fields = ["id", "version_number", "content", "date_created"]


class AcceptedTermsSerializer(serializers.ModelSerializer):

    terms_and_conditions = TermsAndConditionsSerializer(read_only=True)

    class Meta:
        model = AcceptedTerms
        fields = ("id", "terms_and_conditions", "date_accepted")


class BusinessTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessType
        fields = ["id", "name"]


class RelatedUserSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "name", "email"]


class TraderSerializer(DynamicFieldsMixin, NestedModelSerializer, GeoModelSerializer):

    owner = RelatedUserSerializer(read_only=True, fields=["id"])
    business_types = NestedField(BusinessTypeSerializer, many=True, required=False)
    accepted_terms = AcceptedTermsSerializer(read_only=True, many=True)
    location = GeometryField(required=False)

    class Meta:
        model = Trader
        exclude = ("date_created", "date_modified")
        extra_kwargs = {
            "is_visible": {"allow_null": True},
            "currency": {"allow_null": True},
            "order_notes_required": {"allow_null": True},
        }
        read_only_fields = ("id", "number")


class GroupSerializer(serializers.ModelSerializer):

    users = RelatedUserSerializer(many=True, read_only=True, source="user_set")

    class Meta:
        model = TraderGroup
        exclude = ("permissions",)
