from rest_access_policy import AccessPolicy


class TraderAccessPolicy(AccessPolicy):

    statements = [
        {"action": ["list"], "principal": ["*"], "effect": "allow"},
        {
            "action": [
                "create",
                "business_types",
                "terms_conditions",
                "currency_options",
            ],
            "principal": "authenticated",
            "effect": "allow",
        },
        {
            "action": ["destroy"],
            "principal": "authenticated",
            "effect": "allow",
            "condition": ["user_must_be:owner"],
        },
        {
            "action": ["retrieve"],
            "principal": "authenticated",
            "effect": "allow",
            "condition_expression": [
                "(user_must_be:owner or has_perm:trader.view_trader)"
            ],
        },
        {
            "action": ["update", "partial_update"],
            "principal": "authenticated",
            "effect": "allow",
            "condition_expression": [
                "(user_must_be:owner or has_perm:trader.change_trader)"
            ],
        },
        {
            "action": ["search_users", "groups"],
            "principal": "authenticated",
            "effect": "allow",
            "condition_expression": [
                "(user_must_be:owner or has_perm:trader.manage_members)"
            ],
        },
    ]

    def user_must_be(self, request, view, action, field):
        trader = view.get_object()
        return getattr(trader, field) == request.user

    @classmethod
    def scope_queryset(cls, request, qs):

        if request.user.is_superuser or request.trader_mode:
            return qs

        return qs.filter(is_visible=True)

    def get_user_group_values(self, user):
        return list(user.groups.values_list("name", flat=True))
