from rest_framework_nested import routers
from django.contrib import admin
from django.urls import path, include
from traders import views
from categories import views as cat_views
from products import views as prod_views
from orders import views as order_views


router = routers.SimpleRouter(trailing_slash=False)
router.register(r"traders", views.TraderViewSet, basename="traders")

order_router = routers.NestedSimpleRouter(router, r"traders", lookup="trader")
order_router.register(r"orders", order_views.OrderViewSet, basename="orders")

category_router = routers.NestedSimpleRouter(router, r"traders", lookup="trader")
category_router.register(
    r"categories", cat_views.CategoryViewSet, basename="categories"
)

product_router = routers.NestedSimpleRouter(
    category_router, r"categories", lookup="category"
)
product_router.register(r"products", prod_views.ProductViewSet, basename="products")


app_name = "traders"

urlpatterns = [
    path("", include(router.urls)),
    path("", include(category_router.urls)),
    path("", include(order_router.urls)),
    path("", include(product_router.urls)),
]
