from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver
from .utils import unique_trader_number_generator
from .models import Trader, TraderGroup


@receiver(pre_save, sender=Trader)
def save_number(sender, instance, **kwargs):

    if instance._state.adding:
        instance.number = unique_trader_number_generator(instance, 8)


@receiver(post_delete, sender=Trader)
def delete_account_images(sender, instance, **kwargs):
    if instance.avatar:
        instance.avatar.delete(save=False)

    if instance.cover_image:
        instance.cover_image.delete(save=False)


@receiver(post_save, sender=TraderGroup)
def on_create_trader_group(sender, instance, created, **kwargs):

    if created:
        trader = instance.trader
        if trader is not None:
            trader.create_group_permissions(instance)
