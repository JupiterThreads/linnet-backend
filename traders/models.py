from django.contrib.gis.db import models
from django.conf import settings
from djmoney.models.fields import MoneyField, CurrencyField
from djmoney.money import Money
from imagekit import processors
from imagekit.processors import ResizeToFill, ResizeToFit, Thumbnail
from imagekit.models import ProcessedImageField, ImageSpecField
from djmoney.settings import CURRENCY_CHOICES
from core.utils import image_path
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission
from django.contrib.auth.models import Group
import re
import copy


class TraderGroupManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(name__iregex=TraderGroup.SUFFIX_REGEX)

    def get_or_create(self, **kwargs):

        created = False
        get_kwargs = copy.deepcopy(kwargs)

        if "name" in get_kwargs and "trader" in get_kwargs:
            trader = get_kwargs.get("trader")
            name = get_kwargs.get("name")
            get_kwargs["name"] = trader.construct_group_name(name)

        if "trader" in get_kwargs:
            del get_kwargs["trader"]

        try:
            obj = TraderGroup.objects.get(**get_kwargs)
        except TraderGroup.DoesNotExist:
            obj = TraderGroup.objects.create(**kwargs)
            created = True

        return obj, created

    def create(self, **kwargs):

        if "trader" not in kwargs:
            raise Exception("Trader instance is required")

        trader = kwargs.pop("trader")
        name = kwargs.get("name")
        kwargs["name"] = trader.construct_group_name(name)
        return super().create(**kwargs)


class TraderGroup(Group):

    ALLOWED_GROUP_NAMES = ["admin", "content_admin", "read"]
    SUFFIX_REGEX = r"_\d+$"
    EXCLUDE_SUFFIX_REGEX = r"^[^\d]+$"

    objects = TraderGroupManager()

    class Meta:
        proxy = True

    @property
    def suffix(self):
        regex = re.compile(self.SUFFIX_REGEX)
        result = regex.search(self.name)
        if result is not None:
            return result.group()
        return None

    @property
    def trader_id(self):
        suffix = self.suffix
        if suffix is not None:
            return int(suffix.replace("_", ""))
        return 0

    @property
    def trader(self):
        trader_id = self.trader_id
        return Trader.objects.filter(id=trader_id).first()

    def add_users(self, trader, users):
        # A user will never be in more than one group for one trader
        for user in users:
            user.remove_user_from_trader_groups(trader)
        self.user_set.add(*users)


class BusinessType(models.Model):

    name = models.CharField(max_length=255, unique=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name


class TermsAndConditions(models.Model):

    version_number = models.CharField(max_length=10)
    date_created = models.DateTimeField(auto_now_add=True)
    content = models.TextField()

    def __str__(self):
        return "Version {} {}".format(self.version_number, self.date_created)

    @staticmethod
    def get_latest_conditions():
        return TermsAndConditions.objects.order_by("date_created").last()


class AcceptedTerms(models.Model):

    terms_and_conditions = models.ForeignKey(
        TermsAndConditions, on_delete=models.SET_NULL, null=True
    )
    trader = models.ForeignKey(
        "Trader", on_delete=models.CASCADE, related_name="accepted_terms_conditions"
    )
    date_accepted = models.DateTimeField(auto_now_add=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["trader", "terms_and_conditions"], name="unique-accepted-terms"
            )
        ]

    def __str__(self):
        return f"{self.trader} {self.terms_and_conditions}"


class VisibleTraderManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_visible=True)


class Trader(models.Model):

    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="owned_traders"
    )
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255)
    number = models.CharField(max_length=255, blank=True, unique=True)
    is_visible = models.BooleanField(default=False)
    currency = CurrencyField(default="GBP", choices=CURRENCY_CHOICES)
    order_notes_label = models.CharField(max_length=150, blank=True, null=True)
    order_notes_required = models.BooleanField(default=False)
    place_id = models.CharField(max_length=255, blank=True, null=True, unique=True)
    avatar = ProcessedImageField(
        upload_to=image_path,
        blank=True,
        null=True,
        processors=[processors.Transpose(), ResizeToFit(150, 150)],
        format="JPEG",
        options={"quality": 99},
    )
    cover_image = ProcessedImageField(
        upload_to=image_path,
        blank=True,
        null=True,
        processors=[processors.Transpose(), ResizeToFill(500, 500)],
        format="JPEG",
        options={"quality": 99},
    )

    business_types = models.ManyToManyField(
        BusinessType, blank=True, related_name="traders_for_business_types"
    )
    accepted_terms = models.ManyToManyField(
        TermsAndConditions,
        through=AcceptedTerms,
        blank=True,
        related_name="traders_for_accepted_terms",
    )
    location = models.PointField(blank=True, null=True)

    visible_objects = VisibleTraderManager()
    objects = models.Manager()

    def __str__(self):
        return "{} ({})".format(self.name, self.number)

    class Meta:
        permissions = (
            ("manage_all_content", "Can manage all content"),
            ("manage_members", "Can manage members"),
            ("view_all_content", "Can view all content"),
        )

    @property
    def base_s3_path(self):
        return f"linnet/traders/{self.id}"

    @property
    def group_name_regex(self):
        return r"{}$".format(self.group_name_suffix)

    def construct_group_name(self, prefix):
        return f"{prefix}{self.group_name_suffix}"

    @property
    def groups(self):
        return TraderGroup.objects.filter(name__regex=self.group_name_regex)

    @property
    def group_name_suffix(self):
        return f"_{self.id}"

    @property
    def admin_group_name(self):
        return f"admin{self.group_name_suffix}"

    @property
    def content_admin_group_name(self):
        return f"content_admin{self.group_name_suffix}"

    @property
    def read_group_name(self):
        return f"read{self.group_name_suffix}"

    @property
    def all_group_users(self):
        from users.models import User

        user_ids = self.groups.values_list("user", flat=True)
        return User.objects.filter(id__in=user_ids)

    @classmethod
    def admin_permissions(cls):
        content_type = ContentType.objects.get_for_model(cls)

        exclude_perm_codes = ["add_trader", "delete_trader"]

        return Permission.objects.filter(content_type=content_type).exclude(
            codename__in=exclude_perm_codes
        )

    @classmethod
    def content_admin_permissions(cls):
        content_type = ContentType.objects.get_for_model(cls)

        exclude_perm_codes = [
            "add_trader",
            "delete_trader",
            "view_trader",
            "change_trader",
            "manage_members",
        ]

        return Permission.objects.filter(content_type=content_type).exclude(
            codename__in=exclude_perm_codes
        )

    @classmethod
    def read_permissions(cls):
        content_type = ContentType.objects.get_for_model(cls)
        return Permission.objects.filter(
            content_type=content_type, codename="view_all_content"
        )

    def create_group_permissions(self, group):
        from guardian.shortcuts import assign_perm

        if group.name == self.admin_group_name:
            for perm in self.admin_permissions():
                assign_perm(perm, group, self)

        elif group.name == self.content_admin_group_name:
            for perm in self.content_admin_permissions():
                assign_perm(perm, group, self)

        elif group.name == self.read_group_name:
            for perm in self.read_permissions():
                assign_perm(perm, group, self)
