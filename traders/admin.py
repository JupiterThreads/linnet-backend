from django.contrib import admin
from django.contrib.gis.admin import OSMGeoAdmin
from .models import Trader, BusinessType, TermsAndConditions, AcceptedTerms, TraderGroup


class TraderAdmin(OSMGeoAdmin):

    exclude = ("number",)


admin.site.register(Trader, TraderAdmin)
admin.site.register(BusinessType)
admin.site.register(TermsAndConditions)
admin.site.register(AcceptedTerms)
admin.site.register(TraderGroup)
