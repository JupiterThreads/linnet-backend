from django_restql.mixins import DynamicFieldsMixin
from django_restql.serializers import NestedModelSerializer
from django_restql.mixins import EagerLoadingMixin
from django_restql.fields import NestedField
from rest_framework import serializers
from rest_framework.exceptions import ParseError
from .models import Product, Variation, Option
from traders.serializers import TraderSerializer


class VariationSerializer(NestedModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = Variation
        fields = (
            "id",
            "name",
            "description",
            "display_order",
            "unit_price",
            "unit_price_currency",
            "is_published",
        )
        extra_kwargs = {
            "unit_price_currency": {"required": False},
            "description": {"allow_null": True},
        }


class ProductSerializer(EagerLoadingMixin, NestedModelSerializer):

    variations = VariationSerializer(many=True, required=False)

    prefetch_related = {"variations": "variations"}

    class Meta:
        model = Product
        exclude = ("category", "date_created")

    def create(self, validated_data):

        variations = validated_data.pop("variations", None)
        product = Product.objects.create(**validated_data)

        if variations:
            vars = [Variation(product=product, **var_data) for var_data in variations]
            Variation.objects.bulk_create(vars)
        return product

    def update(self, instance, validated_data):

        vars = validated_data.pop("variations", [])

        # Delete vars not present in request
        var_ids = [var["id"] for var in vars if "id" in var]
        removed_vars = instance.variations.exclude(id__in=var_ids)
        if removed_vars:
            removed_vars.delete()

        for var in vars:
            id = var["id"] if "id" in var else None
            var["product"] = instance
            Variation.objects.update_or_create(id=id, defaults=var)

        return super(ProductSerializer, self).update(instance, validated_data)


class OptionSerializer(NestedModelSerializer):

    trader = NestedField(TraderSerializer, write_only=True, accept_pk_only=True)
    products = NestedField(ProductSerializer, many=True, required=False)

    class Meta:
        model = Option
        exclude = ("date_created",)
