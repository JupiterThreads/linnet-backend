from django.shortcuts import get_object_or_404
from core.pagination import CustomCursorPagination
from rest_framework.decorators import action
from .models import Product
from rest_framework import viewsets, filters
from rest_access_policy import AccessViewSetMixin
from .access_policies import ProductAccessPolicy
from .serializers import ProductSerializer
from categories.models import Category


class ProductViewSet(AccessViewSetMixin, viewsets.ModelViewSet):

    serializer_class = ProductSerializer
    access_policy = ProductAccessPolicy
    pagination_class = CustomCursorPagination
    search_fields = ["name"]
    filter_backends = [filters.SearchFilter]
    lookup_field = "id"

    def initial(self, request, *args, **kwargs):
        category_id = self.kwargs["category_id"]
        self.category = get_object_or_404(Category, id=category_id)
        super(ProductViewSet, self).initial(request, *args, **kwargs)

    def get_queryset(self):

        queryset = self.category.products.all().order_by("display_order")
        return self.access_policy.scope_queryset(
            self.request,
            queryset,
        )

    def perform_create(self, serializer):
        serializer.save(category=self.category)

    # @action(methods=['post'], detail=False, url_path='update-order', url_name='update_order')
    # def update_order(self, request, **kwargs):
    #     from core.notifications.product_notifications import update_product_display_order

    #     product_id = request.data['product_id']
    #     new_index = request.data['new_index']

    #     product = Product.objects.get(id=product_id)
    #     old_index = product.display_order
    #     product.update_product_display_order(new_index)

    #     async_to_sync(update_product_display_order)(
    #         product, self.request.user, old_index, new_index)
    #     return Response(status=status.HTTP_204_NO_CONTENT)
