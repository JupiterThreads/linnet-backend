# from core.notifications_utils import get_data, send_broadcast
# from .serializers import ProductSerializer


# async def add_product(product):

#     content = {
#         "payload": ProductSerializer(product).data,
#         "type": 'ADD_PRODUCT',
#     }
#     await broadcast_product_change(product, content)


# async def update_product(product):

#     content = {
#         "payload": ProductSerializer(product).data,
#         "type": 'UPDATE_PRODUCT',
#     }
#     await broadcast_product_change(product, content)


# async def remove_product(product):

#     content = {
#         "payload": str(product._id),
#         "type": 'REMOVE_PRODUCT',
#     }
#     await broadcast_product_change(product, content)


# async def update_product_display_order(product, user, old_index, new_index):

#     content = {
#         "payload": {
#             'old_index': old_index,
#             'new_index': new_index,
#         },
#         "type": 'UPDATE_PRODUCT_DISPLAY_ORDER',
#         "action_user_id": str(user._id),
#     }

#     await broadcast_product_change(product, content)


# async def broadcast_product_change(product, content):

#     content['trader_id'] = str(product.category.trade_account._id)
#     content['category_id'] = str(product.category._id)

#     data = get_data(content)
#     await send_broadcast(data, product.category.trade_account)


# async def update_variation_display_order(variation, product, user, old_index, new_index):

#     content = {
#         'payload': {
#             'old_index': old_index,
#             'new_index': new_index,
#         },
#         'type': 'UPDATE_VARIATION_DISPLAY_ORDER',
#         'action_user_id': str(user._id),
#     }

#     await broadcast_variation_change(variation, product, content)


# async def broadcast_variation_change(variation, product, content):

#     content['trader_id'] = str(product.category.trade_account._id)
#     content['product_id'] = str(product._id)

#     data = get_data(content)
#     await send_broadcast(data, product.category.trade_account)
