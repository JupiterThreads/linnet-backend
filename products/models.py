from django.db import models
from categories.models import Category
from djmoney.models.fields import MoneyField, CurrencyField
from imagekit import processors
from imagekit.processors import ResizeToFill, ResizeToFit, Thumbnail
from imagekit.models import ProcessedImageField, ImageSpecField
from core.utils import image_path
from traders.models import Trader


class Option(models.Model):

    ONE = 1
    MANY = 5
    UP_TO = 10
    AT_LEAST = 15
    EXACTLY = 20

    SELECTION_TYPE_CHOICES = (
        (ONE, "Select one"),
        (MANY, "Select many"),
        (UP_TO, "Select up to"),
        (AT_LEAST, "Select at least"),
        (EXACTLY, "Select"),
    )

    trader = models.ForeignKey(Trader, on_delete=models.CASCADE, related_name="options")
    title = models.CharField(max_length=255, null=True, blank=True)
    selection_type = models.IntegerField(choices=SELECTION_TYPE_CHOICES, default=1)
    selection_number = models.PositiveSmallIntegerField(blank=True, null=True)
    is_published = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    products = models.ManyToManyField(
        "Product", related_name="option_items", blank=True
    )

    def __str__(self):
        return self.title


class Product(models.Model):

    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name="products"
    )
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)
    unit_price = MoneyField(
        max_digits=19, decimal_places=2, default=0.00, default_currency="GBP"
    )
    date_created = models.DateTimeField(auto_now_add=True)
    display_order = models.PositiveSmallIntegerField(blank=True, null=True)
    is_published = models.BooleanField(default=False)
    image = ProcessedImageField(
        upload_to=image_path,
        blank=True,
        null=True,
        processors=[processors.Transpose(), ResizeToFit(400, 400, False)],
        format="JPEG",
        max_length=255,
        options={"quality": 99},
    )
    image_thumbnail = ImageSpecField(
        source="image",
        processors=[ResizeToFit(100, 100)],
        format="JPEG",
        options={"quality": 99},
    )

    options = models.ManyToManyField(
        Option,
        blank=True,
        through=Option.products.through,
        related_name="products_for_option",
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["name", "category"], name="unique_product")
        ]

    def __str__(self):
        return self.name

    @property
    def base_s3_path(self):
        return f"{self.category.base_s3_path}/product_{self.id}"

    @staticmethod
    def get_next_display_order(category):
        last_item = (
            category.products.all()
            .only("display_order")
            .order_by("display_order")
            .last()
        )
        if last_item is not None:
            return last_item.display_order + 1
        return 0

    def update_product_display_order(self, new_index):

        self.display_order = new_index
        self.save(update_fields=["display_order"])

        queryset = (
            self.category.products.exclude(id=self.id)
            .only("id", "display_order")
            .order_by("display_order")
        )
        product_list = list(queryset.values_list("id", flat=True))
        product_list.insert(new_index, self.id)

        for prod in queryset.iterator():
            new_order = product_list.index(prod.id)

            if prod.display_order != new_order:
                prod.display_order = new_order
                prod.save(update_fields=["display_order"])


class Variation(models.Model):

    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name="variations"
    )
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)
    unit_price = MoneyField(
        max_digits=19, decimal_places=2, default=0.00, default_currency="GBP"
    )
    date_created = models.DateTimeField(auto_now_add=True)
    display_order = models.PositiveSmallIntegerField(blank=True, null=True)
    is_published = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["name", "product"], name="unique_variation")
        ]

    def __str__(self):
        return f"{self.product} - {self.name}"

    def update_display_order(self, new_index, product):

        self.display_order = new_index
        self.save(update_fields=["display_order"])

        queryset = (
            product.variations.exclude(id=self.id)
            .only("id", "display_order")
            .order_by("display_order")
        )
        variation_list = list(queryset.values_list("id", flat=True))
        variation_list.insert(new_index, self.id)

        for var in queryset.iterator():
            new_order = variation_list.index(var.id)

            if var.display_order != new_order:
                var.display_order = new_order
                var.save(update_fields=["display_order"])

    @staticmethod
    def get_next_display_order(product):
        last_item = (
            product.variations.all()
            .only("display_order")
            .order_by("display_order")
            .last()
        )
        if last_item is not None:
            return last_item.display_order + 1
        return 0
