from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver

# # Product signals

# @receiver(post_save, sender=Product)
# def send_product_to_client(sender, instance, created, **kwargs):
#     if created:
#         async_to_sync(add_product)(instance)
#     else:
#         async_to_sync(update_product)(instance)


# @receiver(post_delete, sender=Product)
# def notify_product_removed(sender, instance, **kwargs):
#     async_to_sync(remove_product)(instance)


# @receiver(pre_save, sender=Product)
# def set_product_display_order(sender, instance, **kwargs):

#     if instance._state.adding:
#         instance.display_order = Product.get_next_display_order(instance.category)
