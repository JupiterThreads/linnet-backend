from rest_access_policy import AccessPolicy


class ProductAccessPolicy(AccessPolicy):

    statements = [
        {"action": ["list", "retrieve"], "principal": "*", "effect": "allow"},
        {
            "action": ["create"],
            "principal": "authenticated",
            "effect": "allow",
            "condition_expression": [
                "(is_trader_owner or has_perm:trader.manage_all_content)"
            ],
        },
        {
            "action": ["update", "destroy", "partial_update", "update_order"],
            "principal": "authenticated",
            "effect": "allow",
            "condition_expression": [
                "(is_trader_owner or has_perm:trader.manage_all_content)"
            ],
        },
    ]

    @classmethod
    def scope_queryset(cls, request, qs):

        if request.user.is_superuser:
            return qs

        if request.trader_mode:
            if (
                request.user.has_perm("view_all_content", request.trader)
                or request.trader.owner == request.user
            ):
                return qs

        return qs.filter(is_published=True)
