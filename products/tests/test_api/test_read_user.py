import pytest
from pytest_common_subject import precondition_fixture

from pytest_drf import (
    ViewSetTest,
    Returns200,
    Returns403,
    UsesGetMethod,
    UsesDeleteMethod,
    UsesDetailEndpoint,
    UsesListEndpoint,
    UsesPatchMethod,
    UsesPostMethod,
    AsUser,
)
from pytest_lambda import lambda_fixture, static_fixture
from pytest_drf.util import pluralized
from products.models import Product
from products.serializers import ProductSerializer
from djangorestframework_camel_case.util import camelize


def express_product(product):
    data = ProductSerializer(product).data
    return camelize(data)


express_products = pluralized(express_product)


@pytest.mark.django_db()
class TestAdminUser(ViewSetTest):

    list_url = lambda_fixture(
        lambda trader, category: f"/api/traders/{trader.id}/categories/{category.id}/products"
    )

    detail_url = lambda_fixture(
        lambda trader, category, product: f"/api/traders/{trader.id}/categories/{category.id}/products/{product.id}"
    )

    class TestNonPublishedList(
        UsesGetMethod, UsesListEndpoint, Returns200, AsUser("read_user")
    ):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        products = lambda_fixture(
            lambda category: [
                Product.objects.create(
                    category=category, name=obj["name"], unit_price=obj["unit_price"]
                )
                for obj in [
                    {
                        "name": "Hells",
                        "unit_price": "1.98",
                    },
                    {"name": "Peroni", "unit_price": "2.10"},
                    {
                        "name": "San Miguel",
                        "unit_price": "1.50",
                    },
                ]
            ]
        )

        def test_it_returns_products(self, products, results):
            expected = express_products(sorted(products, key=lambda c: c.id))
            actual = sorted(results, key=lambda d: d["id"])
            assert expected == actual

    class TestCreate(UsesPostMethod, UsesListEndpoint, Returns403, AsUser("read_user")):

        data = lambda_fixture(
            lambda: {
                "name": "Hells",
                "unit_price": "1.98",
            }
        )

        initial_product_ids = precondition_fixture(
            lambda category: set(category.products.all().values_list("id", flat=True))
        )

        def test_create_new_product(self, initial_product_ids, category, json):

            expected = initial_product_ids
            actual = set(category.products.all().values_list("id", flat=True))
            assert expected == actual

    class TestRetrieve(
        UsesGetMethod, UsesDetailEndpoint, Returns200, AsUser("read_user")
    ):
        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        product = lambda_fixture(
            lambda category: Product.objects.create(category=category, name="Hells")
        )

        def test_it_returns_product(self, product, json):
            expected = express_product(product)
            actual = json
            assert expected == actual

    class TestUpdate(
        UsesPatchMethod, UsesDetailEndpoint, Returns403, AsUser("read_user")
    ):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        product = lambda_fixture(
            lambda category: Product.objects.create(
                category=category, name="Hells", unit_price="1.98"
            )
        )

        data = static_fixture(
            {
                "name": "Hells Larger",
                "unit_price": "2.10",
            }
        )

        def test_it_does_not_update_product(self, product, json):
            pass

    class TestDestroy(
        UsesDeleteMethod, UsesDetailEndpoint, Returns403, AsUser("read_user")
    ):
        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        product = lambda_fixture(
            lambda category: Product.objects.create(
                category=category, name="Carling", unit_price="1.10"
            )
        )

        initial_product_ids = precondition_fixture(
            lambda category: list(category.products.all().values_list("id", flat=True))
        )

        def test_it_does_not_delete_product(
            self, initial_product_ids, category, product
        ):

            expected = initial_product_ids or []
            expected.append(product.id)
            actual = list(category.products.all().values_list("id", flat=True))
            assert expected == actual
