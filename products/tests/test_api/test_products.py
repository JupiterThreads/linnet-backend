import pytest
from pytest_common_subject import precondition_fixture

from pytest_drf import (
    ViewSetTest,
    Returns200,
    Returns201,
    Returns204,
    Returns404,
    Returns403,
    UsesGetMethod,
    UsesDeleteMethod,
    UsesDetailEndpoint,
    UsesListEndpoint,
    UsesPatchMethod,
    UsesPostMethod,
    AsUser,
    APIViewTest,
)
from pytest_lambda import lambda_fixture, static_fixture
from pytest_drf.util import pluralized
from traders.models import Trader
from products.models import Product
from products.serializers import ProductSerializer
from djangorestframework_camel_case.util import camelize


def express_product(product):
    data = ProductSerializer(product).data
    return camelize(data)


express_products = pluralized(express_product)


@pytest.mark.django_db()
class TestProductViewSet(ViewSetTest):

    list_url = lambda_fixture(
        lambda trader, category: f"/api/traders/{trader.id}/categories/{category.id}/products"
    )

    detail_url = lambda_fixture(
        lambda trader, category, product: f"/api/traders/{trader.id}/categories/{category.id}/products/{product.id}"
    )

    class TestPublishedList(
        UsesGetMethod,
        UsesListEndpoint,
        Returns200,
    ):

        products = lambda_fixture(
            lambda category: [
                Product.objects.create(
                    category=category,
                    name=obj["name"],
                    unit_price=obj["unit_price"],
                    is_published=True,
                )
                for obj in [
                    {
                        "name": "Hells",
                        "unit_price": "1.98",
                    },
                    {"name": "Peroni", "unit_price": "2.10"},
                    {
                        "name": "San Miguel",
                        "unit_price": "1.50",
                    },
                ]
            ]
        )

        def test_it_returns_published_products(self, products, results):
            expected = express_products(sorted(products, key=lambda c: c.id))
            actual = sorted(results, key=lambda d: d["id"])
            assert expected == actual

    class TestNotPublishedList(
        UsesGetMethod,
        UsesListEndpoint,
        Returns200,
    ):

        products = lambda_fixture(
            lambda category: [
                Product.objects.create(category=category, name=obj["name"])
                for obj in [
                    {
                        "name": "Hells",
                        "unit_price": "1.98",
                    },
                    {"name": "Peroni", "unit_price": "2.10"},
                    {
                        "name": "San Miguel",
                        "unit_price": "1.50",
                    },
                ]
            ]
        )

        def test_it_returns_empty_list(self, products, results):
            expected = []
            actual = results
            assert expected == actual

    class TestCreate(UsesPostMethod, UsesListEndpoint, Returns201, AsUser("michael")):

        data = lambda_fixture(
            lambda: {
                "name": "Hells",
                "unit_price": "1.98",
            }
        )

        initial_product_ids = precondition_fixture(
            lambda category: set(category.products.all().values_list("id", flat=True))
        )

        def test_create_new_product(self, initial_product_ids, category, json):

            expected = initial_product_ids | {json["id"]}
            actual = set(category.products.all().values_list("id", flat=True))
            assert expected == actual
            assert "Hells" == json["name"]

    class TestRetrievePublished(
        UsesGetMethod,
        UsesDetailEndpoint,
        Returns200,
    ):

        product = lambda_fixture(
            lambda category: Product.objects.create(
                category=category, name="Hells", unit_price="1.98", is_published=True
            )
        )

        def test_it_returns_product(self, product, json):
            expected = express_product(product)
            actual = json
            assert expected == actual

    class TestRetrieveNonPublished(
        UsesGetMethod,
        UsesDetailEndpoint,
        Returns404,
    ):

        product = lambda_fixture(
            lambda category: Product.objects.create(
                category=category, name="Hells", unit_price="1.98"
            )
        )

        def test_it_returns_product(self, product, json):
            pass

    class TestRetrieveNonPublishedForAuthUser(
        UsesGetMethod, UsesDetailEndpoint, Returns200, AsUser("michael")
    ):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        product = lambda_fixture(
            lambda category: Product.objects.create(
                category=category, name="Hells", unit_price="1.98"
            )
        )

        def test_it_returns_product(self, product, json):
            expected = express_product(product)
            actual = json
            assert expected == actual

    class TestUpdate(
        UsesPatchMethod, UsesDetailEndpoint, Returns200, AsUser("michael")
    ):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        product = lambda_fixture(
            lambda category: Product.objects.create(
                category=category, name="Hells", unit_price="1.98"
            )
        )

        data = static_fixture(
            {
                "name": "Hells Larger",
                "unit_price": "2.10",
            }
        )

        def test_it_updates_product(self, product, json):
            product.refresh_from_db()
            expected = express_product(product)
            actual = json
            assert expected == actual

    class TestDestroy(
        UsesDeleteMethod, UsesDetailEndpoint, Returns204, AsUser("michael")
    ):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        product = lambda_fixture(
            lambda category: Product.objects.create(
                category=category, name="Carling", unit_price="1.10"
            )
        )

        initial_product_ids = precondition_fixture(
            lambda category: set(category.products.all().values_list("id", flat=True))
        )

        def test_deletes_product(self, initial_product_ids, category, product):

            expected = initial_product_ids - {product.id}
            actual = set(category.products.all().values_list("id", flat=True))
            assert expected == actual
