import pytest
from pytest_common_subject import precondition_fixture

from pytest_drf import (
    ViewSetTest,
    Returns200,
    Returns201,
    Returns204,
    Returns404,
    Returns403,
    UsesGetMethod,
    UsesDeleteMethod,
    UsesDetailEndpoint,
    UsesListEndpoint,
    UsesPatchMethod,
    UsesPostMethod,
    AsUser,
    APIViewTest,
)
from pytest_lambda import lambda_fixture, static_fixture
from pytest_drf.util import pluralized
from traders.models import Trader
from products.models import Product, Variation
from products.serializers import ProductSerializer, VariationSerializer
from djangorestframework_camel_case.util import camelize


def express_product(product):
    data = ProductSerializer(product).data
    return camelize(data)


express_products = pluralized(express_product)

product = lambda_fixture(
    lambda category: Product.objects.create(
        category=category, name="Peroni", is_published=True
    ),
    autouse=True,
)

variations = lambda_fixture(
    lambda product: [
        Variation.objects.create(
            product=product, name=obj["name"], unit_price=obj["unit_price"]
        )
        for obj in [
            {"name": "1 pint", "unit_price": "2.10"},
            {"name": "half a pint", "unit_price": "1.60"},
            {"name": "With lemon", "unit_price": "1.80"},
        ]
    ],
    autouse=True,
)


@pytest.mark.django_db()
class TestVariations(ViewSetTest):

    list_url = lambda_fixture(
        lambda trader, category: f"/api/traders/{trader.id}/categories/{category.id}/products"
    )

    detail_url = lambda_fixture(
        lambda trader, category, product: f"/api/traders/{trader.id}/categories/{category.id}/products/{product.id}"
    )

    class TestCreate(UsesPostMethod, UsesListEndpoint, Returns201, AsUser("michael")):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        data = lambda_fixture(
            lambda: {
                "name": "Hells",
                "unit_price": "1.98",
                "variations": [
                    {"name": "Half a pint"},
                    {"name": "1 pint"},
                    {"name": "With lemon"},
                ],
            }
        )

        def test_it_creates_product_w_variations(self, json):

            _product = Product.objects.get(name="Hells")
            expected = express_product(_product)
            actual = json
            assert expected == actual

    class TestUpdate(
        UsesPatchMethod, UsesDetailEndpoint, Returns200, AsUser("michael")
    ):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        data = lambda_fixture(
            lambda product: {
                "name": "Peroni blonde",
                "variations": [
                    {"id": product.variations.all()[0].id, "unitPrice": "3.20"},
                    {"id": product.variations.all()[1].id, "unitPrice": "2.20"},
                ],
            }
        )

        def test_it_updates_product_and_variations(self, product, json):
            product.refresh_from_db()
            expected = express_product(product)
            actual = json
            assert expected == actual

    class TestRemove(
        UsesPatchMethod, UsesDetailEndpoint, Returns200, AsUser("michael")
    ):

        headers = lambda_fixture(lambda: {"TRADER_MODE": "true"})

        data = lambda_fixture(lambda product: {"variations": []})

        def test_it_removes_variations(self, product, json):
            product.refresh_from_db()
            expected = express_product(product)
            actual = json
            assert expected == actual
            assert len(product.variations.all()) == 0
