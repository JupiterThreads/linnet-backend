import pytest
from pytest_lambda import lambda_fixture
from users.models import User
from traders.models import Trader, TraderGroup
from categories.models import Category


michael = lambda_fixture(
    lambda: User.objects.create(
        name="Michael Scott",
        email="michael.scott@dunder.com",
    ),
    autouse=True,
)


trader = lambda_fixture(
    lambda michael: Trader.objects.create(
        name="The office bar", is_visible=True, owner=michael
    )
)


category = lambda_fixture(
    lambda trader: Category.add_root(
        instance=Category(name="Drinks", trader=trader, is_published=True)
    )
)


@pytest.fixture(autouse=True)
def admin_user(trader):
    user = User.objects.create(name="Jan", email="jan.levinson@dunder.com")
    group = TraderGroup.objects.create(name="admin", trader=trader)
    group.user_set.add(user)
    return user


@pytest.fixture(autouse=True)
def cadmin_user(trader):
    user = User.objects.create(name="Stanley Hudson", email="stan.hudson@dunder.com")
    group = TraderGroup.objects.create(name="content_admin", trader=trader)
    group.user_set.add(user)
    return user


@pytest.fixture(autouse=True)
def read_user(trader):
    user = User.objects.create(name="Ryan Howard", email="ryan.howard@dunder.com")
    group = TraderGroup.objects.create(name="read", trader=trader)
    group.user_set.add(user)
    return user
