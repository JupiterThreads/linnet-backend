from django.test import TestCase
from traders.models import Trader
from categories.models import Category
from products.models import Product, Option


class OptionTestCase(TestCase):
    def setUp(self):

        owner = User.objects.create(name="Jamie Oliver")
        self.account = Trader.objects.create(name="Fat Hippo", owner=owner)
        category = Category.objects.create(name="Burgers", trade_account=self.account)
        self.parent = Product.objects.create(name="Beef Patty", category=category)

    def test_representation(self):
        """Test the str representation"""

        main_sides = Option.objects.create(title="Main Side")
        self.assertEqual(str(main_sides), "Main Side")

    def test_adding_options(self):
        """adding options to product"""

        main_sides = Option.objects.create(title="Main Side")
        extra_pattie = Option.objects.create(title="Extra pattie")

        self.parent.options.add(main_sides, extra_pattie)
        expected_list = [main_sides, extra_pattie]
        self.assertQuerysetEqual(
            list(self.parent.options.all()), expected_list, transform=lambda x: x
        )

    def test_adding_items_to_options(self):
        """adding products to options"""

        main_sides = Option.objects.create(title="Main Side")
        self.parent.options.add(main_sides)

        skinny_fries = Product.objects.create(name="Skinny Fries")
        cajun_fries = Product.objects.create(name="Cajun Fries")
        waffle_fries = Product.objects.create(name="Waffle Fries")

        expected_list = [skinny_fries, cajun_fries, waffle_fries]
        main_sides.products.add(*expected_list)

        self.assertQuerysetEqual(
            list(main_sides.products.all()), expected_list, transform=lambda x: x
        )
