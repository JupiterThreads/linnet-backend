from django.test import TestCase
from products.models import Product
from categories.models import Category

# from core.tests.testing_utils import populate_test_db, refresh_from_db
from unittest import skip


@skip("Will be changed")
class ProductTestCase(TestCase):

    category = None
    item_amstel = None
    item_calsberg = None
    item_fosters = None

    def setUp(self):
        # populate_test_db()
        self.category = Category.objects.filter(name="Beers").first()
        self.item_amstel = Product.objects.create(
            name="Amstel", category=self.category, unit_price="2.50"
        )
        self.item_calsberg = Product.objects.create(
            name="Calsberg", category=self.category, unit_price="2.20"
        )
        self.item_fosters = Product.objects.create(
            name="Fosters", category=self.category, unit_price="1.50"
        )
        self.child_item1 = Product.objects.create(name="1 pint")

    def test_representation(self):
        """Test product item representation"""
        self.assertEqual(str(self.item_amstel), self.item_amstel.name)

    def test_base_s3_path(self):
        """Test item image path"""
        self.assertEqual(
            self.item_amstel.base_s3_path,
            f"{self.category.base_s3_path}/item_{self.item_amstel.id}",
        )

    def test_update_product_list_display_order(self):
        """Test update product list display order"""

        self.assertEqual(self.item_amstel.display_order, 0)
        self.assertEqual(self.item_calsberg.display_order, 1)
        self.assertEqual(self.item_fosters.display_order, 2)

        item_ids = [
            str(self.item_fosters.id),
            str(self.item_calsberg.id),
            str(self.item_amstel.id),
        ]

        queryset = self.category.products.filter(parent_item__isnull=True)

        Product.update_product_list_display_order(item_ids, queryset)

        # refresh_from_db([
        #     self.item_calsberg,
        #     self.item_amstel,
        #     self.item_fosters,
        # ])

        self.assertEqual(self.item_fosters.display_order, 0)
        self.assertEqual(self.item_amstel.display_order, 2)
        self.assertEqual(self.item_calsberg.display_order, 1)
