from django.contrib import admin
from .models import Product, Variation, Option


class VariationInLine(admin.TabularInline):

    model = Variation


class OptionInLine(admin.TabularInline):

    model = Product.options.through


class ProductAdmin(admin.ModelAdmin):

    inlines = (VariationInLine, OptionInLine)


admin.site.register(Product, ProductAdmin)
admin.site.register(Variation)
admin.site.register(Option)
