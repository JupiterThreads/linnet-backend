# from channels.layers import get_channel_layer
# from core.models import WebSocketClient
# from users.models improt User
# from channels.db import database_sync_to_async


# def get_data(content):
#     return {
#         "type": "notify",
#         "content": content
#     }


# async def send_broadcast(payload, trader):
#     channel_layer = get_channel_layer()
#     payload["content"].update({"is_broadcast": True})
#     await channel_layer.group_send(trader.group_name, payload)


# async def send_message(payload, user):
#     channel_layer = get_channel_layer()
#     payload["content"].update({"is_broadcast": False})
#     await channel_layer.group_send(user.group_name, payload)


# async def add_user_to_group(user, group_name):

#     client = await get_websocket_client(user)

#     if client is None:
#         return

#     channel_layer = get_channel_layer()
#     await channel_layer.group_add(
#         group_name,
#         client.channel_name
#     )


# async def remove_user_from_group(user, group_name):

#     client = await get_websocket_client(user)

#     if client is None:
#         return

#     channel_layer = get_channel_layer()
#     await channel_layer.group_discard(
#         group_name,
#         client.channel_name
#     )


# @database_sync_to_async
# def get_users(user_ids):
#     return LinnetUser.objects.filter(_id__in=user_ids)


# @database_sync_to_async
# def get_websocket_client(user):

#     try:
#         return WebSocketClient.objects.filter(user=user).latest('created_date')
#     except WebSocketClient.DoesNotExist:
#         pass
#     return None

# # async def update_order(instance):
# #     serializer = OrderSerializer(instance)
# #     content = {
# #         "type": "UPDATE",
# #         "model": "Order",
# #         "payload": serializer.data,
# #     }

# #     data = get_data(content)
# #     await send_to_group(data, user=instance.customer, trader=instance.trade_account)
