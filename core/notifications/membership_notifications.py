# from core.notifications.utils import get_data, remove_user_from_group, \
#     add_user_to_group, send_broadcast, send_message
# from core.serializers import UserMembershipSerializer, TraderMembershipSerializer


# async def add_membership_account(member):

#     _type = 'ADD_MEMBERSHIP_ACCOUNT'

#     serializer = UserMembershipSerializer(member)
#     content = {
#         "payload": serializer.data,
#         "type": _type,
#         "user_id": str(member.user.id),
#     }

#     data = get_data(content)
#     await add_user_to_group(member.user, member.trade_account.group_name)
#     await send_message(data, member.user)
#     await broadcast_member_change(member, content, _type)


# async def remove_membership_account(member):

#     _type = "REMOVE_MEMBERSHIP_ACCOUNT"

#     content = {
#         "payload": str(member.id),
#         "type": _type,
#         "user_id": str(member.user.id),
#     }

#     data = get_data(content)
#     await remove_user_from_group(member.user, member.trade_account.group_name)
#     await send_message(data, member.user)
#     await broadcast_member_change(member, content, _type)


# async def update_membership_account(member):

#     _type = "UPDATE_MEMBERSHIP_ACCOUNT"

#     serializer = UserMembershipSerializer(member)
#     content = {
#         "payload": serializer.data,
#         "type": _type,
#         "user_id": str(member.user.id),
#     }

#     data = get_data(content)
#     await send_message(data, member.user)
#     await broadcast_member_change(member, content, _type)


# async def broadcast_member_change(member, content, type):

#     content["trader_id"] = str(member.trade_account.id)

#     if type != 'REMOVE_MEMBERSHIP_ACCOUNT':
#         serializer = TraderMembershipSerializer(member)
#         content["payload"] = serializer.data

#     data = get_data(content)
#     await send_broadcast(data, member.trade_account)
