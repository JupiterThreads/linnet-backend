#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import json
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from .models import WebSocketClient
from typing import TypeVar, Generic


class NotificationConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self) -> None:
        self.user = self.scope["user"]
        await self.accept()
        if self.user.is_authenticated:
            await self.add_websocket_client()
            await self.add_all_groups()

    async def notify(self, event):
        """
        This handles calls elsewhere in this codebase that look
        like:

            channel_layer.group_send(group_name, {
                'type': 'notify',  # This routes it to this handler.
                'content': json_message,
            })

        Don't try to directly use send_json or anything; this
        decoupling will help you as things grow.
        """
        await self.send_json(event["content"])

    async def receive_json(self, content, **kwargs):
        """
        This handles data sent over the wire from the client.

        We need to validate that the received data is of the correct
        form. You can do this with a simple DRF serializer.

        We then need to use that validated data to confirm that the
        requesting user (available in self.scope["user"] because of
        the use of channels.auth.AuthMiddlewareStack in routing) is
        allowed to subscribe to the requested object.
        """
        print(kwargs)
        print("received content....")
        print(content)
        serializer = self.get_serializer(data=content)

        print(serializer)
        if not serializer.is_valid():
            return
        # Define this method on your serializer:
        group_name = serializer.get_group_name()
        # The AsyncJsonWebsocketConsumer parent class has a
        # self.groups list already. It uses it in cleanup.
        self.groups.append(group_name)
        # This actually subscribes the requesting socket to the
        # named group:
        await self.channel_layer.group_add(
            group_name,
            self.channel_name,
        )

    def get_serializer(self, *, data):
        pass
        # … omitted for brevity. See
        # https://github.com/encode/django-rest-framework/blob/master/rest_framework/generics.py

    async def disconnect(self, close_code):
        if self.user.is_authenticated:
            await self.remove_all_groups()
            await self.remove_websocket_client()

    async def add_all_groups(self):

        await self.channel_layer.group_add(self.user.group_name, self.channel_name)

        for group_name in self.user.get_trade_account_group_names():
            await self.channel_layer.group_add(group_name, self.channel_name)

    async def remove_all_groups(self):

        await self.channel_layer.group_discard(self.user.group_name, self.channel_name)

        for group_name in self.user.get_trade_account_group_names():
            await self.channel_layer.group_discard(group_name, self.channel_name)

    @database_sync_to_async
    def add_websocket_client(self):
        WebSocketClient.objects.create(user=self.user, channel_name=self.channel_name)

    @database_sync_to_async
    def remove_websocket_client(self):
        WebSocketClient.objects.filter(channel_name=self.channel_name).delete()
