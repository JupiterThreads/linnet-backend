from core.utils import get_codename


def has_perm(request, view, action, perm):

    if request.user.has_perm(perm):
        return True

    trader = request.trader

    if trader is None:
        obj = view.get_object()
        if obj.__class__.__name__ == "Trader":
            trader = obj
        else:
            return False

    codename = get_codename(perm)
    return request.user.has_perm(codename, trader)


def is_trader_owner(request, view, action, *args):

    if request.trader is None:
        return False

    return request.user == request.trader.owner
