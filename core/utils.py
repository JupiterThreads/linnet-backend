import random
import uuid
import string
from datetime import date


def random_string_generator(n=6):
    return "".join(
        random.SystemRandom().choice(string.ascii_uppercase + string.digits)
        for _ in range(n)
    )


def random_num_string_generator(n=6):
    return "".join(["{}".format(random.randint(0, 9)) for num in range(0, n)])


def unique_order_number_generator():

    today = date.today()
    str_today = str(today).replace("-", "")

    rand_str = random_string_generator(5)
    order_number = f"{str_today}-{rand_str}"
    return order_number


def unique_trade_number_generator(instance, n=6):
    new_number = random_num_string_generator(n)
    _Class = instance.__class__

    qs_exists = _Class.objects.filter(number=new_number).exists()
    if qs_exists:
        return unique_order_number_generator(instance, n)
    return new_number


def create_uuid_str():
    return str(uuid.uuid4())


def update_instance_image(instance, new_image, field_name):
    current_image = getattr(instance, field_name)
    if new_image is None and current_image:
        current_image.delete(save=False)
    elif new_image is not None:
        if new_image.name != current_image.name:
            current_image.delete(save=False)
        setattr(instance, field_name, new_image)
    return instance


def save_new_image(instance, field_name, image):
    setattr(instance, field_name, image)
    instance.save(update_fields=[field_name])
    return instance


def image_path(instance, filename):
    try:
        path = f"{instance.base_s3_path}/{filename}"
    except AttributeError as error:
        raise error
    return path


def get_codename(perm):

    try:
        app_label, codename = perm.split(".", 1)
    except ValueError:
        raise ValueError(
            "Perm argument must be in" " format: 'app_label.codename' (is %r)" % perm
        )
    return codename
