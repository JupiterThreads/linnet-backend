# # /usr/bin/python3

from django.db import models
from django.conf import settings


class WebSocketClient(models.Model):

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="sockets_for_user",
    )
    channel_name = models.CharField(max_length=255)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} - {}".format(self.channel_name, self.user.name)
