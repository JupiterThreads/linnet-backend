from django.contrib import admin
from django.contrib.auth.models import Group
from guardian.models import UserObjectPermission, GroupObjectPermission
from .models import WebSocketClient
from traders.models import TraderGroup


class GroupAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        return Group.objects.filter(name__iregex=TraderGroup.EXCLUDE_SUFFIX_REGEX)


admin.site.register(WebSocketClient)
admin.site.register(UserObjectPermission)
admin.site.register(GroupObjectPermission)
admin.site.unregister(Group)
admin.site.register(Group, GroupAdmin)
