from django.test import TestCase
from users.models import User
from traders.models import Trader
from core.utils import image_path


class UtilsTestCase(TestCase):

    file_name = "monty.jpg"
    account_image_path_prefix = ""

    def setUp(self):
        owner = User.objects.create(name="Michael Scott", email="m.scott@gmail.com")
        self.account = Trader.objects.create(name="The Office", owner=owner)
        self.account_image_path_prefix = f"linnet/traders/{self.account.id}"

    def test_trade_account_avatar_path(self):
        """Test avatar path for trade account instance"""
        path = image_path(self.account, self.file_name)
        self.assertEqual(
            path, f"{self.account_image_path_prefix}/avatar_{self.file_name}"
        )

    def test_trade_account_cover_image_path(self):
        """Test cover image path for trade account instance"""
        path = image_path(self.account, self.file_name)
        self.assertEqual(
            path, f"{self.account_image_path_prefix}/cover_image_{self.file_name}"
        )

    def test_image_path(self):
        """Test image path"""
        path = image_path(self.account, self.file_name)
        self.assertEqual(
            path, f"{self.account_image_path_prefix}/image_{self.file_name}"
        )
