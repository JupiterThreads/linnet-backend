from rest_framework.authtoken.models import Token
from traders.models import Trader
from users.models import User
from categories.models import Category
from products.models import Product, Variation, Option


BASE_URL = "/api/v1"


def create_credentials(instance, user, trader_mode=False):
    token = Token.objects.get(user=user)
    mode = "true" if trader_mode else "false"
    instance.client.credentials(
        HTTP_AUTHORIZATION=f"Token {token.key}", HTTP_TRADER_MODE=mode
    )


def clear_credentials(instance):
    instance.client.credentials()


# def create_admin_user_and_add_credentials(instance, account):
#     user = create_admin_user(account)
#     create_credentials(instance, user, trader_mode=True)
#     return user


# def create_write_user_and_add_credentials(instance, account):
#     user = create_write_user(account)
#     create_credentials(instance, user, trader_mode=True)
#     return user


# def create_read_user_and_add_credentials(instance, account):
#     user = create_read_user(account)
#     create_credentials(instance, user, trader_mode=True)
#     return user


def set_owner_and_add_credentials(instance, account):
    user = account.owner
    create_credentials(instance, user, trader_mode=True)
    return user


def create_basic_user_and_add_credentials(instance):
    user = create_basic_user()
    create_credentials(instance, user)
    return user


def create_basic_user():
    return User.objects.create(
        name="Kevin Malone", email="kevin.malone@dunder.com"
    )


# def create_read_user(account, name=None, email=None):
#     name = name or "Pam Beesley"
#     email = email or "pam.beesley@dunder.com"
#     user = User.objects.create(name=name, email=email)
#     permission, _ = Permission.objects.get_or_create(name="read", level=0)
#     ta_user, _ = account.get_or_create_member(user)
#     account.update_member_permission(ta_user, permission)
#     return user


# def create_write_user(account, name=None, email=None):
#     name = name or "Dwight Schrute"
#     email = email or "dwight.schrute@dunder.com"
#     user = User.objects.create(name=name, email=email)
#     permission, _ = Permission.objects.get_or_create(name="write", level=1)
#     ta_user, _ = account.get_or_create_member(user)
#     account.update_member_permission(ta_user, permission)
#     return user


# def create_admin_user(account, name=None, email=None):
#     name = name or "Jim"
#     email = email or "jim.halpert@dunder.com"
#     user = User.objects.create(name=name, email=email)
#     permission, _ = Permission.objects.get_or_create(name="admin", level=2)
#     ta_user, _ = account.get_or_create_member(user)
#     account.update_member_permission(ta_user, permission)
#     return user


def create_burger_items(category):
    american = Product.objects.create(
        name="American", category=category, is_published=True, unit_price=10.90
    )
    wild = Product.objects.create(
        name="Wild Bill", category=category, is_published=True, unit_price=11.90
    )
    tony = Product.objects.create(
        name="Swiss Tony", category=category, is_published=True, unit_price=11.90
    )
    pb = Product.objects.create(
        name="PB&Double J", category=category, is_published=True, unit_price=11.90
    )
    items = [american, wild, tony, pb]
    variations = create_variations()
    options = create_options()

    for item in items:
        item.variations.add(*variations)
        item.options.add(*options)
    return items


def create_variations():
    double = Variation.objects.create(
        name="Extra large", unit_price=15.90, is_published=True
    )
    with_salad = Variation.objects.create(
        name="With salad", unit_price=13.90, is_published=True
    )
    with_gherkins = Variation.objects.create(
        name="Standard with gherkins", unit_price=13.90
    )
    return [double, with_salad, with_gherkins]


def create_options():
    main_sides = Option.objects.create(
        title="Main Sides", selection_type=1, selection_number=1, is_published=True
    )
    skinny_fries = Product.objects.create(
        name="Skinny Fries", is_published=True, unit_price=0.50
    )
    cajun_fries = Product.objects.create(
        name="Cajun Fries", is_published=True, unit_price=0.50
    )
    waffle_fries = Product.objects.create(
        name="Waffle Fries", is_published=True, unit_price=1.50
    )
    main_sides.products.add(skinny_fries, cajun_fries, waffle_fries)
    patties = Option.objects.create(
        title="Extra Pattie", selection_type=1, selection_number=1, is_published=True
    )
    beef = Product.objects.create(
        name="Extra Beef Patty", is_published=True, unit_price=3.00
    )
    chicken = Product.objects.create(
        name="Extra Chicken", is_published=True, unit_price=2.50
    )
    vegan = Product.objects.create(
        name="Extra VFC Patty", is_published=True, unit_price=3.00
    )
    patties.products.add(beef, chicken, vegan)
    return [main_sides, patties]


def create_basic_user_list():
    angela = User.objects.create(
        name="Angela Martin", email="angela.martin@dunder.com"
    )
    creed = User.objects.create(
        name="Creed Bratton", email="creed.bratton@dunder.com"
    )
    andy = User.objects.create(
        name="Andy Bernard", email="andy.bernard@duner.com"
    )
    return [angela, creed, andy]


# def create_permissions():
#     Permission.objects.create(name="read", level=0)
#     Permission.objects.create(name="write", level=1)
#     Permission.objects.create(name="admin", level=2)


def populate_fat_hippo():
    user = User.objects.create(
        name="Michael Scott", email="michael.scott@fathippo.com"
    )
    account = Trader.objects.create(
        name="The Fat Hippo", owner=user, is_visible=True
    )
    beef = Category.objects.create(
        name="Beef", trade_account=account, is_published=True
    )
    create_burger_items(beef)


def refresh_from_db(obj_list):
    """Refresh list of objs from db"""
    for obj in obj_list:
        obj.refresh_from_db()
